<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
 
class Song_model extends CI_Model
{
    const STATUS = [
        1 => '顯示',
        0 => '不顯示'
    ];
	
    function __construct()
    {
		parent::__construct();
	}
	
    function get_song_list($data = "")
    {
		$this->db->select('s.*,(select title from song_category as c where s.category_id=c.id) as category_name');
        $this->db->from('song as s');

        if(!empty($data['category_id']) && $data['category_id']!="All"){
            $this->db->like('s.category_id', $data['category_id']);
        } elseif(empty($data['category_id'])){
            $this->db->like('s.category_id', 1);
        }

        if(!empty($data['tag_1'])){
            $this->db->where('s.tag_1', $data['tag_1']);
        }

        if(!empty($data['tag_3'])){
            $this->db->where('s.tag_3', $data['tag_3']);
        }

        if(!empty($data['home']) && $data['home']!='All'){
            $this->db->where('s.home', $data['home']);
        }

		return $this->db->get();
	}

    function get_song_latest($data="")
    {
		$this->db->select('*');
		$this->db->from('song');

        if(!empty($data['home'])){
            $this->db->where('home',$data['home']);
        }

        if(!empty($data['category_id'])){
            $this->db->where('category_id', $data['category_id']);
        }
        if(!empty($data['status'])){
            $this->db->where('status',$data['status']);
        }
        $this->db->where('category_id !=',6);
        $this->db->order_by("created_at", "desc");
        
        if(!empty($data['limit'])){
            $this->db->limit($data['limit']);
        }
		return $this->db->get();
	}

    function get_song_special()
    {
        $this->db->select('*');
        $this->db->from('song');

        $this->db->where('home',1);
        $this->db->where('status',1);
        $this->db->where('category_id',6);
        
        $this->db->limit(1);
        return $this->db->get();
    }
    
    function get_song_one($id="")
    {
        $this->db->query('update song set views=views+1 where id='.$id);
		$this->db->select('s.*,(select title from song_category as c where s.category_id=c.id) as category_name');
		$this->db->from('song as s');
		$this->db->where('s.id',$id);
		return $this->db->get();
	}

    function get_song_with_favorite($data="")
    {
        $this->db->select('s.*,(select u.song_id from user_favorite as u where u.song_id=s.id) as favoriteID,(select title from song_category as c where s.category_id=c.id) as category_name');
        $this->db->from('song as s');
        $this->db->where('s.id',$data['id']);
        return $this->db->get();
    }

    //收藏歌曲
    function get_song_with_collection($user_id=""){
        $this->db->select('u.created_at,s.title,s.author,s.id,s.tag_1,s.tag_3');
        $this->db->from('user_favorite as u');
        $this->db->join('song as s', 's.id = u.song_id');
        $this->db->where('u.user_id',$user_id);
        $this->db->order_by("u.created_at", "desc");
        return $this->db->get();
    }
	
    function get_song_by_category($data="")
    {
		$this->db->select('s.*,(select count(*) from user_favorite as uf where uf.song_id=s.id) as favorite');
		$this->db->from('song as s');
        //,(select count(*) from song as so where so.category_id=s.id and so.status=1) as qty
        //增加點擊數量
        if(!empty($data['category_id'])){
            $this->db->like('s.category_id', $data['category_id']);
            $this->db->query('update song_category set views=views+1 where id='.$data['category_id']);
        }

        if(!empty($data['status'])){
            $this->db->where('s.status',$data['status']);
        }
        $this->db->order_by("s.tag_1", "asc"); 
        $this->db->order_by("s.title", "asc");
		return $this->db->get();
    }

    function get_song_by_keyword($data="")
    {
		$this->db->select('*');
		$this->db->from('song');
        $this->db->where('status',1);

        if(!empty($data['title'])){
            $this->db->like('title', $data['title']);
            $this->db->or_like('author', $data['title']);
        }
        $this->db->order_by("rank", "asc");	
		return $this->db->get();
	}

    function add_song($data="")
    {
		$results = $this->db->insert('song', $data);

        if(!empty($data['category_id'])){
			$this->db->query('update song_category set qty=qty+1 where id in ('.$data['category_id'].')');
		}
		return $this->db->insert_id();
	}

    function update_song($data,$id)
    {
		$this->db->where('id', $id);
		$results = $this->db->update('song', $data); 
		return $results;	
	}
	
    function delete_song($data="")
    {
        if(!empty($data['category_id'])){
            $this->db->query('update song_category set qty=qty-1 where id in ('.$data['category_id'].')');
        }
        $rs = $this->db->delete('song', array('id' => $data['id'])); 
		return $rs;	
	}

}
?>
