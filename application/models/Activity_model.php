<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Activity_model extends CI_Model{

    function __construct()
    {
		parent::__construct();
	}

    function get_activity_list($data="")
    {
		$this->db->select('*');
		$this->db->from('activity');

        if(isset($data['status'])){
            $this->db->where('status',$data['status']);
        }

        $this->db->order_by("created_at", "desc");

        if(!empty($data['limit'])){
            $this->db->limit($data['limit']);
        }
		return $this->db->get();
	}

    function add_activity($data="")
    {
		$results=$this->db->insert('activity', $data);
		return $this->db->insert_id();
	}

    function get_activity_by_id($id="")
    {
		$this->db->select('*');
		$this->db->from('activity');
		$this->db->where('id',$id);
		return $this->db->get();
	}

    function update_activity($data="",$id="")
    {
		$this->db->where('id', $id);
		$results=$this->db->update('activity',$data);
		return $results;
	}

    function delete_activity($id="")
    {
		$results=$this->db->delete('activity', array('id' => $id));
		return $results;
	}

	function getStoreByDateStoreID($data="")
    {
		$this->db->select('*');
		$this->db->from('activity');
		$this->db->where('storeid', $data['storeid']);
		$this->db->where('DATE(created_at)', $data['today']);

		return $this->db->get();
	}

	function getStoreByDate($data="") {
			$SQL = "select SUM(views) as views,storeid,DATE(created_at) as created_at FROM activity ";
			$WHERE = "WHERE DATE(created_at) BETWEEN '".$data['startdate']."' and '".$data['enddate']."' ";
			$Other = "";
			if (!empty($data['mode']) && $data['mode'] == "month") {
				$Other = "group by storeid,YEAR(created_at),MONTH(created_at)";
			} else {
				$Other = "group by storeid,DATE(created_at)";
			}
			$Other.= " order by created_at";
			$SQL = $SQL.$WHERE.$Other;
			return $this->db->query($SQL);

			// $this->db->select('SUM(views) as views,storeid,DATE(created_at) as created_at');
			// $this->db->from('activity');
			// $this->db->where("DATE(created_at) BETWEEN '".$data['startdate']."' and '".$data['enddate']."'");
			// if (!empty($data['mode']) && $data['mode'] == "month") {
			// 	$this->db->group_by('MONTH(created_at)');
			// } else {
			// 	$this->db->group_by('DATE(created_at)');
			// }
			// $this->db->order_by("created_at", "asc");
			// return $this->db->get();
	}
}
