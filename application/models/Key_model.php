<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Key_model extends CI_Model{
	
    function __construct()
    {
		parent::__construct();
	}
	
	function get_story($id="")
    {
		$this->db->select('*');
		$this->db->from('keyvalue');
		$this->db->where('keyword',$id);
		return $this->db->get();
	}

    function update_story($data="",$id="")
    {
		$this->db->where('id', $id);
		return $this->db->update('keyvalue',$data);
	}

	function get_fold($id="")
    {
		$this->db->select('*');
		$this->db->from('keyvalue');
		$this->db->where('keyword',$id);
		return $this->db->get();
	}

	function update_fold($data="",$id="")
    {
		$this->db->where('id', $id);
		$results=$this->db->update('keyvalue',$data); 
		return $results;	
	}
	
}
