<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Partner_category_model extends CI_Model{
	
    function __construct()
    {
		parent::__construct();
	}
	
    function get_partner_category_list($data="")
    {
		$this->db->select('*');
		$this->db->from('partner_category');
        
        if(!empty($data['title']) ){
            $this->db->like('title', $data['title']);
        }
        
        if(isset($data['status'])){
            $this->db->where('status', $data['status']);
        }

        $this->db->order_by("rank", "asc");	
		return $this->db->get();
	}

	function get_partner_by_id($id="")
    {
		$this->db->select('*');
		$this->db->from('partner_category');
        
        $this->db->where("id",$id);
		return $this->db->get();
	}
	
    function add_partner_category($data="")
    {
		$results = $this->db->insert('partner_category', $data); 
		return $this->db->insert_id();
	}
	
    function update_partner_category($data="", $id="")
    {
		$this->db->where('id', $id);
		$results = $this->db->update('partner_category', $data); 
		return $results;	
	}
	
    function delete_partner_category($id="")
    {
		$results = $this->db->delete('partner_category', array('id' => $id)); 
		return $results;	
	}
}
