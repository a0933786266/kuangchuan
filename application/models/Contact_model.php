<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Contact_model extends CI_Model{
	
    function __construct()
    {
		parent::__construct();
	}
	
    function get_contact_list($data="")
    {
		$this->db->select('*');
		$this->db->from('contact');
		return $this->db->get();
	}
	
    function add_contact($data="")
    {
		$results=$this->db->insert('contact', $data); 
		return $this->db->insert_id();
	}
    
    function update_contact($data="",$id="")
    {
		$this->db->where('id', $id);
		$results=$this->db->update('contact',$data); 
		return $results;	
	}
	
    function delete_contact($id="")
    {
		$results=$this->db->delete('contact', array('id' => $id)); 
		return $results;	
	}
}
