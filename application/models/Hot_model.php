<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Hot_model extends CI_Model{
	
    function __construct()
    {
		parent::__construct();
	}
	
    function get_hot_list($data="")
    {
		$this->db->select('*');
		$this->db->from('hot');

        if(!empty($data['title']) ){
            $this->db->like('title',$data['title']);
        }

        if(isset($data['status'])){
            $this->db->where('status',$data['status']);
        }
        
        if(!empty($data['limit'])){
            $this->db->limit($data['limit']);
        }

		return $this->db->get();
	}
	
    function add_hot($data="")
    {
		$results=$this->db->insert('hot', $data); 
		return $this->db->insert_id();
	}
	
    function get_hot_by_id($id="")
    {
		$this->db->select('*');
		$this->db->from('hot');
		$this->db->where('id',$id);
		return $this->db->get();
	}
    
    function update_hot($data="",$id="")
    {
		$this->db->where('id', $id);
		$results=$this->db->update('hot',$data); 
		return $results;	
	}
	
    function delete_hot($id="")
    {
		$results=$this->db->delete('hot', array('id' => $id)); 
		return $results;	
	}
}
