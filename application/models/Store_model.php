<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Store_model extends CI_Model
{

    function __construct()
    {
		parent::__construct();
	}

    function get_store_list($data = "")
    {
		$this->db->select('s.*,a.name as city_name');
		$this->db->from('store as s');
        $this->db->join('cityarea as a', 's.city = a.id','left');

        if($data['city']!='' && $data['city'] !='All'){
            $this->db->where('s.city', $data['city']);
        }

        if($data['town']!='' && $data['town'] !='All'){
            $this->db->where('s.town', $data['town']);
        }

        if(!empty($data['status'])){
            $this->db->where('s.status', $data['status']);
        }

		return $this->db->get();
	}

    function getAllStore($data = "")
    {
        $this->db->select('s.*');
        $this->db->from('store as s');
        $this->db->order_by("name", "asc");
        return $this->db->get();
    }

    function listEnable()
    {
        $this->db->select('s.*');
        $this->db->from('store as s');
        $this->db->where('status',1);
        $this->db->order_by("name", "asc");
        return $this->db->get();
    }

    function get_store_latest($data="")
    {
		$this->db->select('*');
		$this->db->from('store');

        if(!empty($data['home'])){
            $this->db->where('home',$data['home']);
        }

        if(!empty($data['category_id'])){
            $this->db->where('category_id', $data['category_id']);
        }
        if(!empty($data['status'])){
            $this->db->where('status',$data['status']);
        }
        $this->db->where('category_id !=',6);
        $this->db->order_by("created_at", "desc");

        if(!empty($data['limit'])){
            $this->db->limit($data['limit']);
        }
		return $this->db->get();
	}

    function get_store_one($id="")
    {
		$this->db->select('s.*');
		$this->db->from('store as s');
		$this->db->where('s.id',$id);
		return $this->db->get();
	}

    function get_store_by_category($data="")
    {
		$this->db->select('s.*,(select count(*) from user_favorite as uf where uf.store_id=s.id) as favorite');
		$this->db->from('store as s');
        //,(select count(*) from store as so where so.category_id=s.id and so.status=1) as qty
        //增加點擊數量
        if(!empty($data['category_id'])){
            $this->db->like('s.category_id', $data['category_id']);
            $this->db->query('update store_category set views=views+1 where id='.$data['category_id']);
        }

        if(!empty($data['status'])){
            $this->db->where('s.status',$data['status']);
        }
        $this->db->order_by("s.tag_1", "asc");
        $this->db->order_by("s.title", "asc");
		return $this->db->get();
    }

    function get_store_by_keyword($data="")
    {
		$this->db->select('*');
		$this->db->from('store');
        $this->db->where('status',1);

        if(!empty($data['title'])){
            $this->db->like('title', $data['title']);
            $this->db->or_like('author', $data['title']);
        }
        $this->db->order_by("rank", "asc");
		return $this->db->get();
	}

    function add_store($data="")
    {
		$results = $this->db->insert('store', $data);
		return $this->db->insert_id();
	}

    function view_a_store($id) {
        return $this->db->query('update store set views=views+1 where id='.$id);
    }

    function update_store($data,$id)
    {
		$this->db->where('id', $id);
		$results = $this->db->update('store', $data);
		return $results;
	}

    function delete_store($data="")
    {
        $rs = $this->db->delete('store', array('id' => $data['id']));
		return $rs;
	}

}
?>
