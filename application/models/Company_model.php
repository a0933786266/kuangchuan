<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Company_model extends CI_Model{
	
    function __construct()
    {
		parent::__construct();
	}
	
    function get_company_list($data="")
    {
		$this->db->select('*');
		$this->db->from('company');

        if(!empty($data['limit'])){
            $this->db->limit($data['limit']);
        }

		return $this->db->get();
	}
	
    function get_company_by_uniqid($id="", $detail="")
    {
		$this->db->select('*');
		$this->db->from('company');
        
		$this->db->where('id',$id);
        
		return $this->db->get();
	}
    
    function update_company($data="", $id="")
    {
		$this->db->where('id', $id);
		$results=$this->db->update('company', $data); 
		return $results;	
	}
}
