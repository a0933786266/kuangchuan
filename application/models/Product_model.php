<?php
class Product_model extends CI_Model{
	
	function __construct(){
		parent::__construct();
	}
	
	function get_product_list($data=""){
		$this->db->select('*');
		$this->db->from('products');
        
        if(!empty($data['name']) ){
            $this->db->like('name',$data['name']);
        }
        
        if(isset($data['status'])){
            $this->db->where('status',$data['status']);
        }
        
        if(!empty($data['limit'])){
            $this->db->limit($data['limit']);
        }

        $this->db->order_by("rank", "asc");	
		return $this->db->get();
	}
	
	function add_product($data=""){
		$results=$this->db->insert('products', $data); 
		return $this->db->insert_id();
	}
	
    //取得產品以及細項
    function get_product_by_uniqid($id="",$detail=""){
		$this->db->select('*');
		$this->db->from('products');
        //$this->db->where('pd.main_key',1);
        
		$this->db->where('id',$id);
        
		return $this->db->get();
	}
    
	function update_product($data="",$id=""){
		$this->db->where('id', $id);
		$results=$this->db->update('products',$data); 
		return $results;	
	}
	
	function delete_product($id=""){
		$results=$this->db->delete('products', array('id' => $id)); 
		return $results;	
	}
}
?>