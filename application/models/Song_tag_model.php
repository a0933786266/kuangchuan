<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Song_tag_model extends CI_Model{
	
    function __construct()
    {
		parent::__construct();
	}
	
    function get_song_tag_list($data="")
    {
		$this->db->select('*');
		$this->db->from('song_tag');

        if(!empty($data['title'])){
            $this->db->like('title', $data['title']);
        }

        if(isset($data['parent_id'])){
            $this->db->where('parent_id', $data['parent_id']);
        }
        $this->db->order_by("rank", "asc");
        if(!empty($data['limit'])){
            $this->db->limit($data['limit']);
        }

		return $this->db->get();
	}
	
    function add_song_tag($data="")
    {
		$results = $this->db->insert('song_tag', $data); 
		return $this->db->insert_id();
	}
	
    function get_song_tag_by_one($id="", $detail="")
    {
		$this->db->select('*');
		$this->db->from('song_tag');
        
		$this->db->where('id',$id);
        
		return $this->db->get();
	}
    
    function update_song_tag($data="",$id="")
    {
		$this->db->where('id', $id);
		$results=$this->db->update('song_tag',$data); 
		return $results;	
	}
	
    function delete_song_tag($id="")
    {
		$results=$this->db->delete('song_tag', array('id' => $id)); 
		return $results;	
	}
}
