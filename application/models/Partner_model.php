<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Partner_model extends CI_Model{
	
    function __construct()
    {
		parent::__construct();
	}
	
    function get_partner_list($data="")
    {
		$this->db->select('p.id,p.title,p.link,p.context,p.created_at,(select pc.title from partner_category as pc where pc.id=p.category_id) as category_name');
		$this->db->from('partner as p');

		if(!empty($data['category_id'])){
            $this->db->where('p.category_id',$data['category_id']);
        }
		return $this->db->get();
	}
	
    function add_partner($data="")
    {
        $results = $this->db->insert('partner', $data);
        
        if(!empty($data['category_id'])){
			$this->db->query('update partner_category set qty=qty+1 where id='.$data['category_id']);
		}

		return $this->db->insert_id();
	}
	
    function get_partner_by_id($id="")
    {
		$this->db->select('*');
		$this->db->from('partner');
		$this->db->where('id',$id);
		return $this->db->get();
	}
    
    function update_partner($data="",$id="")
    {
		$this->db->where('id', $id);
		$results = $this->db->update('partner',$data); 
		return $results;	
	}
	
    function delete_partner($id="")
    {
		$results = $this->db->delete('partner', array('id' => $id)); 
		return $results;	
	}
}
