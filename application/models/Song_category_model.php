<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Song_category_model extends CI_Model{
	
    function __construct()
    {
		parent::__construct();
	}
	
    function get_song_category_list($data="")
    {
		$this->db->select('s.*,(select count(*) from song as so where so.category_id=s.id and so.status=1) as qtys');
		$this->db->from('song_category as s');
        
        if(!empty($data['title']) ){
            $this->db->like('s.title', $data['title']);
        }
        
        if(isset($data['status'])){
            $this->db->where('s.status', $data['status']);
        }
        
        if(!empty($data['limit'])){
            $this->db->limit($data['limit']);
        }
        $this->db->order_by("s.rank", "asc");	
		return $this->db->get();
	}
	
    function add_song_category($data="")
    {
		$results = $this->db->insert('song_category', $data); 
		return $this->db->insert_id();
    }

    function get_song_category_one($id)
    {
        $this->db->select('*');
		$this->db->from('song_category');
		$this->db->where('id', $id);
		return $this->db->get();
    }


    function get_song_category_api($data="")
    {
        $this->db->select('*');
        $this->db->from('song_category');

        if(!empty($data['id'])){
            $this->db->where('id', $id);
        }

        return $this->db->get();
    }
	
    function update_song_category($data="", $id="")
    {
		$this->db->where('id', $id);
		$results = $this->db->update('song_category', $data); 
		return $results;	
	}
	
    function delete_song_category($id="")
    {
		$results = $this->db->delete('song_category', array('id' => $id)); 
		return $results;	
	}
}
