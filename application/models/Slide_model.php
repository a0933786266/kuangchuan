<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Slide_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_slide_list($data="")
    {
        $this->db->select('*');
        $this->db->from('slide');

        if(!empty($data['title']) ){
            $this->db->like('title', $data['title']);
        }

        if(isset($data['status'])){
            $this->db->where('status',$data['status']);
        }

        if(!empty($data['limit'])){
            $this->db->limit($data['limit']);
        }

        $this->db->order_by("rank", "asc"); 
        return $this->db->get();
    }

    function add_slide($data="")
    {
        $results = $this->db->insert('slide', $data); 
        return $this->db->insert_id();
    }

    function get_slide_by_uniqid($id="", $detail="")
    {
        $this->db->select('*');
        $this->db->from('slide');

        $this->db->where('id',$id);

        return $this->db->get();
    }

    function update_slide($data="", $id="")
    {
        $this->db->where('id', $id);
        $results = $this->db->update('slide', $data); 
        return $results;    
    }

    function delete_slide($id="")
    {
        $results = $this->db->delete('slide', array('id' => $id)); 
        return $results;    
    }
}
