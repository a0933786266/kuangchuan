<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Notice_model extends CI_Model{
	
    function __construct()
    {
		parent::__construct();
	}
	
    function get_notice_list($data="")
    {
		$this->db->select('*');
		$this->db->from('notice');

        if(!empty($data['title']) ){
            $this->db->like('title',$data['title']);
        }

        if(isset($data['status'])){
            $this->db->where('status',$data['status']);
        }
        
        if(!empty($data['limit'])){
            $this->db->limit($data['limit']);
        }

		return $this->db->get();
	}
	
    function add_notice($data="")
    {
		$results=$this->db->insert('notice', $data); 
		return $this->db->insert_id();
	}
	
    function get_notice_by_uniqid($id="",$detail="")
    {
		$this->db->select('*');
		$this->db->from('notice');
        
		$this->db->where('id',$id);
        
		return $this->db->get();
	}
    
    function update_notice($data="",$id="")
    {
		$this->db->where('id', $id);
		$results=$this->db->update('notice',$data); 
		return $results;	
	}
	
    function delete_notice($id="")
    {
		$results=$this->db->delete('notice', array('id' => $id)); 
		return $results;	
	}
}
