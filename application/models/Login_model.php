<?php
/*
 * 後臺登入
 */
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{
    function __construct()
    {
		parent::__construct();
	}
	
    function admin_login($data)
    {
		$this->db->select('id,name,email');
		$this->db->from('manager');
		$this->db->where('email', $data['email']);
		$this->db->where('password', $data['password']);
		$this->db->limit(1);
		return $this->db->get();
	}
}
