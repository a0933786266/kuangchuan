<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
 
class Share_model extends CI_Model
{
	
    function __construct()
    {
		parent::__construct();
	}
	
    function get_share_list($data="")
    {
		$this->db->select('u.*,(select s.title from song as s where u.song_id=s.id) as title');
		$this->db->from('user_share as u');
		return $this->db->get();
	}
    
    function get_share_latest($data="")
    {
		$this->db->select('*');
		$this->db->from('user_share');
		$this->db->where('home',1);
		$this->db->limit(2);
		return $this->db->get();
	}

    function get_share_one($id)
    {
		$this->db->select('*');
		$this->db->from('user_share');
		$this->db->where('id',$id);
		return $this->db->get();
	}

	function get_shares_by_song($id=""){
		$this->db->select('u.*,(select s.title from song as s where u.song_id=s.id) as title');
		$this->db->from('user_share as u');
		$this->db->where('u.song_id',$id);
		$this->db->where('u.status',1);
		return $this->db->get();
	}

	function add_share($data=""){
		$results=$this->db->insert('user_share', $data); 
		return $this->db->insert_id();
	}
	
    function update_share($data,$id)
    {
		$this->db->where('id', $id);
		$results = $this->db->update('user_share', $data); 
		return $results;	
	}
	
    function delete_share($id)
    {
		$rs = $this->db->delete('user_share', array('id' => $id)); 
		return $rs;	
	}
}
