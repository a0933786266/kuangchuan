<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class City_model extends CI_Model{
	
    function __construct()
    {
		parent::__construct();
	}
	
    function get_city_list()
    {
		$this->db->select('*');
		$this->db->from('cityarea');
		return $this->db->get();
	}
	
    function add_city($data="")
    {
		$results=$this->db->insert('cityarea', $data); 
		return $this->db->insert_id();
	}
	
    function get_city_by_id($id="")
    {
		$this->db->select('*');
		$this->db->from('cityarea');
		$this->db->where('id',$id);
		return $this->db->get();
	}
    
    function update_city($data="",$id="")
    {
		$this->db->where('id', $id);
		$results=$this->db->update('cityarea',$data); 
		return $results;	
	}
	
    function delete_city($id="")
    {
		$results=$this->db->delete('cityarea', array('id' => $id)); 
		return $results;	
	}
}
