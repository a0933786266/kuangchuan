<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class About_model extends CI_Model{
	
    function __construct()
    {
		parent::__construct();
	}
	
    function get_about_list($data="")
    {
		$this->db->select('*');
		$this->db->from('about');

        if(!empty($data['title']) ){
            $this->db->like('title',$data['title']);
        }

        if(isset($data['status'])){
            $this->db->where('status',$data['status']);
        }
        
        if(!empty($data['limit'])){
            $this->db->limit($data['limit']);
        }

		return $this->db->get();
	}
	
    function add_about($data="")
    {
		$results=$this->db->insert('about', $data); 
		return $this->db->insert_id();
	}
	
    function get_about_by_id($id="")
    {
		$this->db->select('*');
		$this->db->from('about');
		$this->db->where('id',$id);
		return $this->db->get();
	}
    
    function update_about($data="",$id="")
    {
		$this->db->where('id', $id);
		$results=$this->db->update('about',$data); 
		return $results;	
	}
	
    function delete_about($id="")
    {
		$results=$this->db->delete('about', array('id' => $id)); 
		return $results;	
	}
}
