<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Manager_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_manager_list()
    {
        $this->db->select('*');
        $this->db->from('manager');
        $data = $this->db->get();
        return $data;
    }

    function get_author_list()
    {
        $this->db->select('*');
        $this->db->from('manager');
        return $this->db->get();
    }

    function get_manager_one($id="")
    {
        $this->db->select('*');
        $this->db->from('manager');
        $this->db->where('id', $id);
        return $this->db->get();
    }

    function add_manager($data)
    {
        $results = $this->db->insert('manager', $data);
        return $results;
    }

    function update_manager($data, $id)
    {
        $this->db->where('id', $id);
        $results = $this->db->update('manager', $data);
        return $results;
    }

    function delete_manager($id)
    {
        $rs = $this->db->delete('manager', array('id' => $id));
        return $rs;
    }
}
