<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
 
class User_model extends CI_Model
{
	
    function __construct()
    {
		parent::__construct();
	}
	
    function get_user_list($data)
    {
		$this->db->select('*');
		$this->db->from('user');
		return $this->db->get();
	}
    
    function get_user_one($id)
    {
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('id',$id);
		return $this->db->get();
	}

	function get_user_by_token($token="")
    {
		$this->db->select('id,name');
		$this->db->from('user');
		$this->db->where('token',$token);
		return $this->db->get();
	}
    
    function get_user_by_email($email)
    {
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('email',$email);
		return $this->db->get();
	}

	function add_user($data=""){
		$results=$this->db->insert('user', $data); 
		return $this->db->insert_id();
	}
	
    function update_user($data,$id)
    {
		$this->db->where('id', $id);
		$results = $this->db->update('user', $data); 
		return $results;	
	}
	
    function delete_user($id)
    {
		$rs = $this->db->delete('user', array('id' => $id)); 
		return $rs;	
	}

	function register_verify($data=""){
        $this->db->select('id,app_id,email');
		$this->db->from('user');
		$this->db->where('app_id',$data['app_id']);
        $this->db->or_where('email', $data['email']);
		return $this->db->get();
    }

    function login_verify($data=""){
    	$this->db->select('id,views,name,app_id,email');
		$this->db->from('user');
		$this->db->where('email',$data['email']);
        $this->db->where('password', $data['password']);
		return $this->db->get();
    }

    function fblogin_verify($data=""){
    	$this->db->select('id,name,app_id,email,views');
		$this->db->from('user');
		$this->db->where('email',$data['email']);
		return $this->db->get();
    }

    function add_song_favorite($data=""){
        $results = $this->db->insert('user_favorite', $data);
        return $this->db->insert_id();
    }

    function add_song_record($data=""){
        $results = $this->db->insert('user_record', $data);
        return $this->db->insert_id();
    }
}
