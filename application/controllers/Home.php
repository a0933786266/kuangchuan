<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller{
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('city_model');
        $this->load->model('store_model');
        $this->load->model('key_model');
    }

    public function index(){
    	$where['city'] =  $this->input->post('city');
        $where['town'] =  $this->input->post('town');
        $where['status'] = 1;
        $citylist = $this->city_model->get_city_list();
        $data['citylist'] = $citylist->result();

        $store = $this->store_model->get_store_list($where);
        $data['store'] = $store->result();


        $key ='fold';
        $fold = $this->key_model->get_fold($key);
        $data['fold'] = $fold->row();


        $key ='story';
        $story = $this->key_model->get_story($key);
        $data['story'] = $story->row();
        
        $this->load->view('home.php', $data);
    }
}
