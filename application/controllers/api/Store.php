<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('store_model');
        $this->load->model('activity_model');
        $this->output->set_header('Access-Control-Allow-Origin: *');  
        $this->output->set_header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS");
        $this->output->set_header('Access-Control-Allow-Headers: content-type');
        $this->output->set_content_type('application/json');
    }

    function getStore()
    {
        $data = array();
        $where = array(
            'city' => $this->input->post('city'),
            'town' => $this->input->post('town'),
            'status'=>1
        );
        
        $store = $this->store_model->get_store_list($where);
        $data['store'] = $store->result();
        $this->output->set_output(json_encode($data));
    }

    function addStore(){
        $data = array();
        $input = array(
            'name' => $this->input->post('name'),
            'manager' => $this->input->post('manager'),
            'address' => $this->input->post('address'),
            'phone' => $this->input->post('phone'),
            'status' => 0
        );

        $rs = $this->store_model->add_store($input);
        
        if($rs!=0) {
            $data['status'] = 200;
        } else {
            $data['status'] = 503;
        }
        $this->output->set_output(json_encode($data));
    }

    function delete_store()
    {
        $where['id'] = $this->input->post('id');
        $rs = $this->store_model->delete_store($where);
        $data = array('code' => $rs);
        $this->output->set_output(json_encode($data));
    }

    //累積瀏覽次數
    function viewStore()
    {
        $data = array();
        $id = $this->input->post('id');
        
        $rs = $this->store_model->view_a_store($id);
        $this->dailyRecord();
        $data['status'] = $rs;
        $this->output->set_output(json_encode($data));
    }

    function dailyRecord() {
        $storeid = $this->input->post('id');
        $where['storeid'] = $storeid;
        $where['today'] = date("Y-m-d");

        $storeRow = $this->activity_model->getStoreByDateStoreID($where);
        $row = $storeRow->row();
        $rownums = $storeRow->num_rows();

        if($rownums == 0) {
            $data['storeid'] = $storeid;
            $data['views'] = 1;
            $this->activity_model->add_activity($data);
        } else {
            $id = $row->id;
            $data['views'] = (int) $row->views+1;
            $this->activity_model->update_activity($data,$id);
        }
    }
}
