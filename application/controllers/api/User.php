<?php
/**
 * API會員
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('user_model');
        $this->output->set_header('Access-Control-Allow-Origin: *');  
		$this->output->set_header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS");
		$this->output->set_header('Access-Control-Allow-Headers: content-type');
		$this->output->set_content_type('application/json');
	}
	
    //收藏歌曲
    function add_song_favorite(){
        $token = $this->input->post('token');
        $user = $this->user_model->get_user_by_token($token);
        
        $row_data = $user->num_rows();
        if($row_data==1){
            $row = $user->row();
            $data = array(
               'song_id' => $this->input->post('song_id'),
               'user_id' => $row->id,
               'keys' => 'favorite'
            );
            $this->user_model->add_song_favorite($data);

            $res['status'] = 200;
        } else {
            $res['status'] = 501;
        }
        
        $this->output->set_output(json_encode($res));
    }
	
	function logout(){
        $this->session->sess_destroy();
        $data = array(
            'message' => '登出系統',
            'status' => 200
        );
        $this->output->set_output(json_encode($data));
    }
    /*
	function userLogin(){
		$data=array(
			'email'=>$this->input->post('email'),
			'password'=>md5($this->input->post('password'))
		);
		
        $user = $this->user_model->login_verify($data);
		$row_data=$user->num_rows();
        
		if($row_data==1){
		    $row = $user->row();
			$userData = array(
				'uid'=>$row->id,
				'name'=>$row->name,
				'email'=>$row->email,
				'is_logged_in'=>true,
                'status'=> 200
			);
			$this->output->set_output(json_encode($userData));
		} else {
			$userData=array(
                '登入失敗',
                'status'=> 510
			);
			$this->output->set_output(json_encode($userData));
		}
	}
    */

    function authorized()
    {
        $res = array();
        $token = $this->input->post('token');
        if(!empty($token)){
            $user = $this->user_model->get_user_by_token($token);
            $row_data = $user->num_rows();
            if($row_data==1){
                $res['status'] = 200;
            } else {
                $res['status'] = 501;
            }
        } else {
            $res['status'] = 401;
        }
        $this->output->set_output(json_encode($res));
    }

    function fbLogin(){
        $res = array();
        $email = $this->input->post('email');

        if(empty($email)){
            $res = array(
                'message'=>'請先點選Facebook登入',
                'status'=> 515
            );
        } else {
            $where['email'] = $email;
            $user = $this->user_model->fblogin_verify($where);
            $row_data = $user->num_rows();
            
            $token = md5(uniqid(rand()));
            if($row_data==1){
                $token = md5(uniqid(rand()));
                $row = $user->row();
                $res = array(
                    'uid' => $row->id,
                    'name' => $row->name,
                    'email' => $row->email,
                    'token' => $token,
                    'status' => 200
                );
                
                //更新token
                $userUpdate = array(
                   'token' => $token,
                   'updated_at' => date('Y-m-d H:i:s'),
                   'views' => (int)$row->views+1
                );
                $this->user_model->update_user($userUpdate,$row->id);
            } elseif($row_data==0){
                $res = array(
                    'message'=>'查無此組帳號，請先進行註冊',
                    'status'=> 403
                );
            }
        }
        $this->output->set_output(json_encode($res));
    }

    function Login(){
        $res = array();
		$data = array();

        $email = $this->input->post('email');
        $password = md5($this->input->post('password'));
        
        if(empty($email) || empty($password)){
            $res = array(
                'message'=>'請輸入Email及密碼',
                'status'=> 515
			);
        } else {
            $data['password']= $password;
            $data['email']= $email;
            
            $user = $this->user_model->login_verify($data);
    		$row_data = $user->num_rows();
            
            $token = md5(uniqid(rand()));
    		if($row_data==1){
    		    $row = $user->row();
    			$res = array(
    				'uid' => $row->id,
    				'name' => $row->name,
                    'email' => $row->email,
                    'token' => $token,
                    'status' => 200
    			);
                
                //更新token
                $userUpdate = array(
        		   'token' => $token,
                   'updated_at' => date('Y-m-d H:i:s'),
                   'views' => (int)$row->views+1
        		);
                
        		$this->user_model->update_user($userUpdate,$row->id);
    		} elseif($row_data>1) {
    		    $res = array(
                    'message'=>'帳號重複',
                    'status'=> 511
    			);
            } elseif($row_data==0){
                $res = array(
                    'message'=>'帳號或密碼錯誤',
                    'status'=> 204
    			);
    		}
        }
        $this->output->set_output(json_encode($res));
	}
    
    public function fbRegister(){
        //尚未註冊
        $data = array();
        $res = array();
        
        $app_id = $this->input->post('app_id');
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        if(!empty($name) && !empty($email)){
            $where['app_id'] = $app_id;
            $where['email'] = $email;
                
            $user = $this->user_model->register_verify($where);
    		$rows = $user->row();
            $userNum = $user->num_rows();
            
            if($userNum>1){
                $res = array(
                    'message'=>'帳號已建立過',
                    'status'=> 511
    		   );
            } else {
                $userid=0;
                $token = md5(uniqid(rand()));
                $input['app_id'] = $app_id;
                $input['name'] = $name;
                $input['email'] = $email;
                $input['token'] = $token;
                $input['password'] = md5($password);

                //僅存在email的時候
                if(!empty($rows->email) && empty($rows->app_id)){
                    $userid = $rows->id;
                    $this->user_model->update_user($input,$rows->id);
                    
                } else if(empty($rows->email) && empty($rows->app_id)){
                    $userid = $this->user_model->add_user($input);
                }
                
                if(!empty($userid)){
                    $res = array(
                        'message'=>'帳號建立完成',
                        'status'=> 200,
                        'uid'=> $userid,
                        'name'=> $name,
                        'email'=> $email,
                        'token'=> $token
        			);
                } else {
                   $res = array(
                        'message'=>'帳號建立失敗',
                        'status'=> 512
        		   ); 
                }
            }
        } else {
            $res = array(
                'message'=>'請填寫完整資訊再進行註冊',
                'status'=> 513
			); 
        }
        $this->output->set_output(json_encode($res));
    } 

    function deleteUser(){
        $data = array(
            'app_id'=>$this->input->post('app_id')
        );
        $res = array();
        
        if($data['app_id']!=''){
            $user = $this->user_model->get_user_by_fbid($data);
            $row_data = $user->num_rows();
            
            if($row_data==1){
                $row = $user->row();
                $rs = $this->user_model->delete_user($row->id);
                if($rs==1){
                    $rs = $this->won_model->delete_won_by_user_id($row->id);
                    $res['status']= 200;
                    $res['message']= '帳號已移除';
                } else{
                    $res['status']= 516;
                    $res['message']= '帳號移除失敗';
                }
            }
        } else{
            $res['status']= 517;
            $res['message']= '請輸入FacebeookID';
        }
        $this->output->set_output(json_encode($res));
    }
}
