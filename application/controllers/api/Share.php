<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Share extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('share_model');
        $this->output->set_header('Access-Control-Allow-Origin: *');  
        $this->output->set_header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS");
        $this->output->set_header('Access-Control-Allow-Headers: content-type');
        $this->output->set_content_type('application/json');
    }

    function add_share_song()
    {
        $res = array();
        $data = array(
            'user_id' => $this->input->post('user_id'),
            'song_id' => $this->input->post('song_id'),
            'name' => $this->input->post('name'),
            'context' => $this->input->post('context'),
            'link' => $this->input->post('link'),
        );

        $id = $this->share_model->add_share($data);
        if($id!=0){
            $res['status'] = 200;
        } else {
            $res['status'] = 501;
        }
        $this->output->set_output(json_encode($res));        
    }

    // 取得該首歌曲的分享
    function get_shares_by_song($song="")
    {
        $shares = $this->share_model->get_shares_by_song($song);
        $data['shares'] = $shares->result();
        $data['status'] = 200;
        $this->output->set_output(json_encode($data));        
    }

    // 首頁中的分享歌曲
    function get_share_latest()
    {
        $share = $this->share_model->get_share_latest();
        $data['shares'] = $share->result();
        $data['status'] = 200;
		$this->output->set_output(json_encode($data));
    }

    function get_share($id="")
    {
        $res = array();
        if(empty($id)){
            $res['status'] = 401;
        } else {
            $share = $this->share_model->get_share_one($id);
            $share = $share->row();
            $res['status'] = 200;
        }
        $this->output->set_output(json_encode($res));
    }
}
