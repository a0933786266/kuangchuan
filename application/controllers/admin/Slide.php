<?php
defined('BASEPATH') or exit('No direct script access allowed');

class slide extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Verify_model');
        $this->load->model('slide_model');
        $this->load->library('FileUpload');
        $this->load->library('general');
    }

    function index()
    {
        $this->general->init_page();
        //get slide 
        $slide = $this->slide_model->get_slide_list();
        $data['slide'] = $slide->result();

        $this->load->view('admin/slide_list.php', $data);
        $this->load->view('admin/footer');
    }

    function adding_slide_page()
    {
        $this->general->init_page();
        
        $this->load->view('admin/slide_adding_page');
        $this->load->view('admin/footer');
    }

    function edit_slide_page($id="")
    {
        $this->general->init_page();
        
        $slide = $this->slide_model->get_slide_by_uniqid($id);
        $slide_row = $slide->row();
        $data['slide'] = $slide_row;
        
        $this->load->view('admin/slide_editing_page', $data);
        $this->load->view('admin/footer');
    }

    function adding_slide()
    {
        $data = array(
            'title' => trim($this->input->post('title')),
        );

        $fileSet = array('path' => 'upload/slide/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }
        
        $this->slide_model->add_slide($data);

        redirect('/admin/Slide');
    }

    function editing_slide()
    {
        $id = $this->input->post('id');
        $data = array(
            'title' => trim($this->input->post('title')),
        );
        
        $fileSet = array('path' => 'upload/slide/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }

        $rs = $this->slide_model->update_slide($data,$id);
        redirect('/admin/Slide/edit_slide_page/'.$id);
    }
    
    function change_ranking()
    {
        $id = $this->input->post('id');
        $data = array(
            'rank' => $this->input->post('rank'),
        );
        $this->slide_model->update_slide($data, $id);
    }
    
    function delete_slide()
    {
        $id = $this->input->post('id');
        $rs = $this->slide_model->delete_slide($id);
        $data = array('code' => $rs);
        $this->output->set_output(json_encode($data));
    }
}
