<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Share extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Verify_model');
        $this->load->model('share_model');
        $this->load->library('general');
    }

    function index()
    {
        $this->general->init_page();
        
        $share = $this->share_model->get_share_list();
        $data['share'] = $share->result();

        $this->load->view('admin/share_list.php', $data);
        $this->load->view('admin/footer');
    }
    
    function share_edit_page($id="")
    {
        $this->general->init_page();
        
        $share = $this->share_model->get_share_by_id($id);
        $data['share'] = $share->row();
        
        $this->load->view('admin/share_editing_page', $data);
        $this->load->view('admin/footer');
    }

    function editing_share()
    {
        $id = $this->input->post('id');
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
            'link' => $this->input->post('link'),
        );
        
        $rs = $this->share_model->update_share($data,$id);
        redirect('/admin/share/share_edit_page/'.$id);
    }
    
    function change_status()
    {
        $id = $this->input->post('id');
        $data = array(
            'status' => $this->input->post('status'),
        );
        $this->share_model->update_share($data, $id);
    }

    function change_home_recommend()
    {
        $id = $this->input->post('id');
        $data = array(
            'home' => $this->input->post('status')
        );
        $this->share_model->update_share($data, $id);

        $data = array(
            'code' => $rs
        );
        $this->output->set_output(json_encode($data));
    }

    function delete_share()
    {
        $id = $this->input->post('id');
        $rs = $this->share_model->delete_share($id);

        $res = array();
        if($rs==1){
            $res['status'] = 200;
        } else {
            $res['status'] = 501;
        }
        
        $this->output->set_output(json_encode($res));
    }
}
