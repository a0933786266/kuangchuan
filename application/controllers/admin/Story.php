<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Story extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Verify_model');
        $this->load->model('key_model');
        $this->load->library('general');
        $this->load->library('FileUpload');
    }

    function editpage()
    {
        $this->general->init_page();
        $key ='story';
        $story = $this->key_model->get_story($key);
        $data['story'] = $story->row();
        
        $this->load->view('admin/story_edit_page', $data);
        $this->load->view('admin/footer');
    }


    function edit()
    {
        $id = $this->input->post('id');
        $fileSet = array('path' => 'upload/data/', 'file_name' => 'mobile');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['val'] = $upload_info['file_name'];
        }

        $fileSet = array('path' => 'upload/data/', 'file_name' => 'desktop');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['val2'] = $upload_info['file_name'];
        }

        if(count($data)>0) {
            $rs = $this->key_model->update_story($data,$id);
        }
        redirect('/admin/Story/editpage/');
    }
   
}
