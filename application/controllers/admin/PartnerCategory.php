<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PartnerCategory extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Verify_model');
        $this->load->model('partner_category_model');
        $this->load->library('general');
        $this->load->library('FileUpload');
    }

    function index()
    {
        $this->general->init_page();
        
        $partner_category = $this->partner_category_model->get_partner_category_list();
        $data['partner_category'] = $partner_category->result();

        $this->load->view('admin/partner_category_list.php', $data);
        $this->load->view('admin/footer');
    }

    function adding_partner_category_page()
    {
        $this->general->init_page();
        $this->load->view('admin/partner_category_adding_page');
        $this->load->view('admin/footer');
    }

    function edit_partner_category_page($id="")
    {
        $this->general->init_page();
        
        $partner_category = $this->partner_category_model->get_partner_by_id($id);
        $data['partner_category'] = $partner_category->row();
        $this->load->view('admin/partner_category_editing_page', $data);
        $this->load->view('admin/footer');
    }

    function adding_partner_category()
    {
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
        );

        $fileSet = array('path' => 'upload/partner/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }
        
        $mainid = $this->partner_category_model->add_partner_category($data);
        redirect('/admin/PartnerCategory');
    }

    function editing_partner_category()
    {
        $id = $this->input->post('id');
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
        );
        
        $fileSet = array('path' => 'upload/partner/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }

        $rs = $this->partner_category_model->update_partner_category($data,$id);
        redirect('/admin/PartnerCategory/edit_partner_category_page/'.$id);
    }
    
    function change_ranking()
    {
        $id = $this->input->post('id');
        $data = array(
            'rank' => $this->input->post('rank'),
        );
        $this->partner_category_model->update_partner_category($data, $id);
    }
    
    function delete_partner_category()
    {
        $id = $this->input->post('id');
        $rs = $this->partner_category_model->delete_partner_category($id);
        $data = array('code' => $rs);
        $this->output->set_output(json_encode($data));
    }
}
