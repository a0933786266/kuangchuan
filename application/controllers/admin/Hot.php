<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hot extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Verify_model');
        $this->load->model('hot_model');
        $this->load->library('FileUpload');
        $this->load->library('general');
    }

    function index()
    {
        $this->general->init_page();
        
        $hot = $this->hot_model->get_hot_list();
        $data['hot'] = $hot->result();

        $this->load->view('admin/hot_list.php', $data);
        $this->load->view('admin/footer');
    }

    function adding_hot_page()
    {
        $this->general->init_page();
        
        $this->load->view('admin/hot_adding_page');
        $this->load->view('admin/footer');
    }

    function edit_hot_page($id="")
    {
        $this->general->init_page();
        
        $hot = $this->hot_model->get_hot_by_id($id);
        $data['hot'] = $hot->row();
        
        $this->load->view('admin/hot_editing_page', $data);
        $this->load->view('admin/footer');
    }

    function adding_hot()
    {
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
            'link' => $this->input->post('link'),
        );

        $fileSet = array('path' => 'upload/hot/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }

        $this->hot_model->add_hot($data);
        redirect('/admin/Hot');
    }

    function editing_hot()
    {
        $id = $this->input->post('id');
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
            'link' => $this->input->post('link'),
        );
        
        $fileSet = array('path' => 'upload/hot/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }

        $rs = $this->hot_model->update_hot($data,$id);
        redirect('/admin/hot/edit_hot_page/'.$id);
    }
    
    function change_status()
    {
        $id = $this->input->post('id');
        $data = array(
            'status' => $this->input->post('status'),
        );
        $this->hot_model->update_hot($data, $id);
    }
    
    function change_ranking()
    {
        $id = $this->input->post('id');
        $data = array(
            'rank' => $this->input->post('rank'),
        );
        $this->hot_model->update_hot($data, $id);
    }
    
    function delete_hot()
    {
        $id = $this->input->post('id');
        $rs = $this->hot_model->delete_hot($id);
        $data = array('code' => $rs);
        $this->output->set_output(json_encode($data));
    }
}
