<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Activity extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Verify_model');
        $this->load->model('activity_model');
        $this->load->library('general');
    }

    function index()
    {
        $this->general->init_page();
        
        $activity = $this->activity_model->get_activity_list();
        $data['activity'] = $activity->result();

        $this->load->view('admin/activity_list.php', $data);
        $this->load->view('admin/footer');
    }

    function activity_adding_page()
    {
        $this->general->init_page();
        
        $this->load->view('admin/activity_adding_page');
        $this->load->view('admin/footer');
    }

    function activity_edit_page($id="")
    {
        $this->general->init_page();
        
        $activity = $this->activity_model->get_activity_by_id($id);
        $data['activity'] = $activity->row();
        
        $this->load->view('admin/activity_editing_page', $data);
        $this->load->view('admin/footer');
    }

    function adding_activity()
    {
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
            'link' => $this->input->post('link'),
        );
        $this->activity_model->add_activity($data);
        redirect('/admin/activity');
    }

    function editing_activity()
    {
        $id = $this->input->post('id');
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
            'link' => $this->input->post('link'),
        );
        
        $rs = $this->activity_model->update_activity($data,$id);
        redirect('/admin/activity/activity_edit_page/'.$id);
    }
    
    function change_status()
    {
        $id = $this->input->post('id');
        $data = array(
            'status' => $this->input->post('status'),
        );
        $this->activity_model->update_activity($data, $id);
    }
    
    function change_ranking()
    {
        $id = $this->input->post('id');
        $data = array(
            'rank' => $this->input->post('rank'),
        );
        $this->activity_model->update_activity($data, $id);
    }
    
    function delete_activity()
    {
        $id = $this->input->post('id');
        $rs = $this->activity_model->delete_activity($id);
        $data = array('code' => $rs);
        $this->output->set_output(json_encode($data));
    }
}
