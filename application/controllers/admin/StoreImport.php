<?php
defined('BASEPATH') or exit('No direct script access allowed');

class StoreImport extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Verify_model');
        $this->load->library('FileUpload');
        $this->load->model('store_model');
        $this->load->model('city_model');
        $this->load->library('general');
    }

    function index()
    {
        $this->general->init_page();
        $this->load->view('admin/storeImport.php');
        $this->load->view('admin/footer');
    }

    function import_data(){
        $this->load->library('phpexcel');
        $data =array();

        $upload_data = array('path' => 'upload/data/', 'file_name' => 'file');
        $upload_info = $this->fileupload->upload_data($upload_data);

        if ($upload_info['status'] !=0) {
            $data['file'] = $upload_info['file_name'];

            $inputFileName = "upload/data/".$data['file'];
            if (!file_exists($inputFileName)) {
                exit("Please run first.".EOL);
            }

            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);

            $citylist = $this->city_model->get_city_list();
            $citys = $citylist->result();

            /*
            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

            foreach($sheetData as $sk =>$sv){
                echo $sv['B'];
            }
            */
            $worksheet = $objPHPExcel->getSheet(0);
            $lastRow = $worksheet->getHighestRow();
            for ($row = 1; $row <= $lastRow; $row++) {
                 
                 $name = $worksheet->getCell('B'.$row)->getValue();
                 $address = $worksheet->getCell('C'.$row)->getValue();
                 
                 $city = mb_substr( $address,0,3,"utf-8");
                 $town =mb_substr($address,3,3,"utf-8");
                 //echo $name;
                 //echo mb_substr( $address,0,3,"utf-8").mb_substr($address,3,3,"utf-8");
                 $phone = $worksheet->getCell('D'.$row)->getValue();
                 
                 foreach($citys as $key => $value) {
                    if($value->name == $city) {
                        $tow =explode("|",$value->road_name);
                        $towKey = 0;
                        for($i=0;$i<count($tow);$i++){
                            if($tow[$i]==$town) {
                                $towKey=$i;
                            }
                        }
                        $data = array(
                            'name' => trim($name),
                            'phone' => $phone,
                            'address' => $address,
                            'city' => $value->id,
                            'town' => $towKey,
                            'status' => 1
                        );
                        $mainid = $this->store_model->add_store($data);
                        //print_r($data);
                        //echo "<br/>";
                    }
                 }

                 
            }
            //redirect('/admin/StoreImport');
        }
    }

    function edit_song_tag_page($id="")
    {
        
    }

    function adding_song_tag()
    {

    }

    function editing_song_tag()
    {
        $id = $this->input->post('id');
        $data = array(
            'title' => trim($this->input->post('title')),
            'parent_id' => $this->input->post('parent_id'),
        );
        
        $rs = $this->song_tag_model->update_song_tag($data, $id);
        redirect('/admin/SongTag/edit_song_tag_page/'.$id);
    }
    
    function delete_song_tag()
    {
        $id = $this->input->post('id');
        $rs = $this->song_tag_model->delete_song_tag($id);
        $data = array('code' => $rs);
        $this->output->set_output(json_encode($data));
    }
}
