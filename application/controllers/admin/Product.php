<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Verify_model');
        $this->load->model('product_model');
        // $this->load->model('company_model');
        $this->load->model('file_model');
        $this->load->library('general');
        $this->load->library('FileUpload');
    }

    function index()
    {
        $this->general->init_page();
           
        $product = $this->product_model->get_product_list();
        $data['product'] = $product->result();

        $this->load->view('admin/product_list.php', $data);
        $this->load->view('admin/footer');
    }

    function adding_product_page()
    {
        $this->general->init_page();

        $company = $this->company_model->get_company_data();
        $data['company'] = $company->result();
        
        $this->load->view('admin/product_adding_page',$data);
        $this->load->view('admin/footer');
    }

    function edit_product_page($id="")
    {
        $this->general->init_page();
        $company = $this->company_model->get_company_data();
        $data['company'] = $company->result();

        $product = $this->product_model->get_product_by_uniqid($id);
        $product_row=$product->row();
        $data['product'] = $product_row;

        $images = $this->file_model->get_file_by_product($id)->result();

        $image_preview = [];
        $image_config = [];
        foreach ($images as $key => $value) {
            $image_preview[] = "/upload/products/" . $value->path;
            $image_config[] = [
                    'caption' => $value->path,
                    'size' => $value->size,
                    'width' => '120px',
                    'url' => '/admin/product/delete_file',
                    'key' => $value->id,
                ];
        }

        $data['images']['preview'] = json_encode($image_preview);
        $data['images']['config'] = json_encode($image_config);
        
        $this->load->view('admin/product_editing_page', $data);
        $this->load->view('admin/footer');
    }

    function adding_product()
    {
        $data = array(
            'name' => trim($this->input->post('name')),
            'cid' => $this->input->post('cid'),
            'price' => $this->input->post('price'),
            'content' => $this->input->post('content'),
        );
        
        $mainid = $this->product_model->add_product($data);

        $data = array(
            'rank' => $mainid,
        );
        $this->product_model->update_product($data, $mainid);

        // 更新指定圖片pid
        $file_ids = $this->input->post('file_id');
        if ($file_ids !=='') {
            $ids = explode(',', $file_ids);
            $ids = array_filter($ids);
            $this->file_model->update_file(array('pid' => $mainid) , $ids);
        }

        redirect('/admin/product');
    }

    function editing_product()
    {
        $id = $this->input->post('id');
        $data = array(
            'name' => trim($this->input->post('name')),
            'cid' => $this->input->post('cid'),
            'price' => $this->input->post('price'),
            'status' => $this->input->post('status'),
            'content' => $this->input->post('content'),
        );
        
        $rs=$this->product_model->update_product($data,$id);
        redirect('/admin/product/edit_product_page/'.$id);
    }
    
    function change_status(){
        $id = $this->input->post('id');
        $data = array(
            'status' => $this->input->post('status'),
        );
        $this->product_model->update_product($data, $id);
    }
    
    function change_ranking(){
        $id = $this->input->post('id');
        $data = array(
            'rank' => $this->input->post('rank'),
        );
        $this->product_model->update_product($data, $id);
        
        $response=array();
        $response['status']=200;
        echo json_encode($response);
    }
  
    function delete_product()
    {
        $id = $this->input->post('id');
        $rs = $this->product_model->delete_product($id);
        $data = array('code' => $rs);
        $this->output->set_output(json_encode($data));
    }

    function add_file($pid = 0)
    {
        $id = null;
        $i = 0;

        $_FILES['filedata']['name'] = $_FILES['file']['name'][$i];
        $_FILES['filedata']['type'] = $_FILES['file']['type'][$i];
        $_FILES['filedata']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
        $_FILES['filedata']['error'] = $_FILES['file']['error'][$i];
        $_FILES['filedata']['size'] = $_FILES['file']['size'][$i];
        
        $fileSet = array('path' => 'upload/products/', 'file_name' => 'filedata');
        $upload_info = $this->fileupload->upload_data($fileSet);

        // FIXME 如果遇到500怎麼辦 e.g. 圖檔太大
        // 新增圖片
        if(!empty($upload_info['file_name'])) {
            $uploadData['pid'] = $pid;
            $uploadData['title'] = $upload_info['client_name'];
            $uploadData['type'] = $upload_info['file_type'];
            $uploadData['path'] = $upload_info['file_name'];
            $uploadData['size'] = $upload_info['file_size'];
            $id = $this->file_model->add_file($uploadData);
        }

        $data = array('id' => $id);
        $this->output->set_output(json_encode($data)); 
    }

    function delete_file()
    {
        $id = $this->input->post('key');
        $rs = $this->file_model->update_file(['pid' => 0], $id);
        $data = array('code' => $rs);
        $this->output->set_output(json_encode($data));
    }
}