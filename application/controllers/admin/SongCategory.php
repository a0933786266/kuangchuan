<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SongCategory extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Verify_model');
        $this->load->model('song_category_model');
        $this->load->library('general');
        $this->load->library('FileUpload');
    }

    function index()
    {
        $this->general->init_page();
        $song_category = $this->song_category_model->get_song_category_list();
        $data['song_category'] = $song_category->result();

        $this->load->view('admin/song_category_list.php', $data);
        $this->load->view('admin/footer');
    }

    function adding_song_category_page()
    {
        $this->general->init_page();
        
        $this->load->view('admin/song_category_adding_page');
        $this->load->view('admin/footer');
    }

    function edit_song_category_page($id="")
    {
        $this->general->init_page();
        
        $song_category = $this->song_category_model->get_song_category_one($id);
        $song_category_row = $song_category->row();
        $data['song_category'] = $song_category_row;
        
        $this->load->view('admin/song_category_editing_page', $data);
        $this->load->view('admin/footer');
    }

    function adding_song_category()
    {
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
            'longcontext' => $this->input->post('longcontext')
        );

        $fileUpload = array('path' => 'upload/song/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileUpload);
        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }

        $fileUpload = array('path' => 'upload/song/', 'file_name' => 'image_origin');
        $upload_info = $this->fileupload->upload_data($fileUpload);
        if(!empty($upload_info['file_name'])) {
            $data['image_origin'] = $upload_info['file_name'];
        }

        redirect('/admin/SongCategory');
    }

    function editing_song_category()
    {
        $id = $this->input->post('id');
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
            'longcontext' => $this->input->post('longcontext'),
        );
        
        $fileSet = array('path' => 'upload/song/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);
        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }

        $fileUpload = array('path' => 'upload/song/', 'file_name' => 'image_origin');
        $upload_info = $this->fileupload->upload_data($fileUpload);
        if(!empty($upload_info['file_name'])) {
            $data['image_origin'] = $upload_info['file_name'];
        }

        $this->song_category_model->update_song_category($data,$id);
        redirect('/admin/SongCategory/edit_song_category_page/'.$id);
    }
    
    function change_status()
    {
        $id = $this->input->post('id');
        $data = array(
            'status' => $this->input->post('status'),
        );
        $this->song_category_model->update_song_category($data, $id);
    }
    
    function delete_song_category()
    {
        $id = $this->input->post('id');
        $rs = $this->song_category_model->delete_song_category($id);
        $data = array('code' => $rs);
        $this->output->set_output(json_encode($data));
    }
}
