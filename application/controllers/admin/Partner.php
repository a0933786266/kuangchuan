<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner extends CI_Controller {

    function __construct()
    {
		parent::__construct();
		$this->load->model('Verify_model');
		$this->load->model('partner_model');
        $this->load->model('partner_category_model');
        $this->load->library('FileUpload');
		$this->load->library('general');
	}

    function index()
    {
		$this->general->init_page();
        
        $partner = $this->partner_model->get_partner_list();
        $data['partner'] = $partner->result();
        
        $this->load->view('admin/partner_list.php',$data);
		$this->load->view('admin/footer');
	}
	
    function adding_partner_page()
    {
        $this->general->init_page();
        $partner_category = $this->partner_category_model->get_partner_category_list();
        $data['partner_category'] = $partner_category->result();

        $this->load->view('admin/partner_adding_page',$data);
        $this->load->view('admin/footer');
    }

    function edit_partner_page($id="")
    {
        $this->general->init_page();
        $partner_category = $this->partner_category_model->get_partner_category_list();
        $data['partner_category'] = $partner_category->result();

        $partner = $this->partner_model->get_partner_by_id($id);
        $partner_row = $partner->row();
        $data['partner'] = $partner_row;
        
        $this->load->view('admin/partner_editing_page', $data);
        $this->load->view('admin/footer');
    }

    function adding_partner()
    {
		$data=array(
			'title'=>$this->input->post('title'),
            'context'=>$this->input->post('context'),
            'link'=>$this->input->post('link'),
            'category_id'=>$this->input->post('category_id')
		);
		$fileSet = array('path' => 'upload/hot/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }

		$rs = $this->partner_model->add_partner($data);
		redirect('/admin/Partner/');
	}
	
    function editing_partner()
    {
		$id=$this->input->post('id');

        $data=array(
			'title'=>$this->input->post('title'),
			'context'=>$this->input->post('context'),
            'link'=>$this->input->post('link'),
            'category_id'=>$this->input->post('category_id')
		);
        
        $fileSet = array('path' => 'upload/hot/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }

		$this->partner_model->update_partner($data, $id);
		redirect('/admin/Partner/');
	}
	
	function delete_partner()
    {
        $id = $this->input->post('id');
        
        $rs = $this->partner_model->delete_partner($id);
        $data = array('code' => $rs);
        
        $this->output->set_output(json_encode($data));
        redirect('/admin/Partner/');
    }
}
