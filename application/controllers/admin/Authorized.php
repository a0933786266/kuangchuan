<?php
/**
 * 管理員Manager
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Authorized extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('general');
	}

    function logout(){
        $this->session->sess_destroy();
        redirect('/admin/');
    }
    
	function login(){
		
		$this->load->model('login_model');
		$data=array(
			'email'=>$this->input->post('email'),
			'password'=>$this->input->post('password')
		);
		$admin=$this->login_model->admin_login($data);
		$row_data=$admin->num_rows();

		if($row_data==1){
			foreach($admin->result() as $row){
				$admin=array(
					'manager_id'=>$row->id,
					'manager_name'=>$row->name,
					'manager_email'=>$row->email,
					'is_logged_in'=>true
				);
				$this->session->set_userdata($admin);
			}
			redirect('/admin/Store');
		} else {
			redirect('/admin');
		}
	}
}
