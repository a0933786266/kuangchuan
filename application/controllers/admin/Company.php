<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Company extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Verify_model');
        $this->load->model('company_model');
        $this->load->library('general');
    }

    function index()
    {
        $this->general->init_page();
        
        $company = $this->company_model->get_company_list();
        $data['company'] = $company->result();

        $this->load->view('admin/company_list.php', $data);
        $this->load->view('admin/footer');
    }

    function edit_company_page($id="")
    {
        $this->general->init_page();
        
        $company = $this->company_model->get_company_by_uniqid($id);
        $company_row = $company->row();
        $data['company'] = $company_row;
        
        $this->load->view('admin/company_editing_page', $data);
        $this->load->view('admin/footer');
    }

    function editing_company()
    {
        $id = $this->input->post('id');
        $data = array(
            'phone' => trim($this->input->post('phone')),
            'fax' => $this->input->post('fax'),
            'meail' => $this->input->post('email'),
            'address' => $this->input->post('address'),
        );
        
        $rs = $this->company_model->update_company($data,$id);
        redirect('/admin/company/edit_company_page/'.$id);
    }
}
