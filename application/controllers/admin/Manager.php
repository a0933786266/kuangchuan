<?php
/*
 * 管理員
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Manager extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Verify_model');
        $this->load->library('general');
        $this->load->model('manager_model');
    }

    public function index()
    {
        $this->general->init_page();
        
        $manager_list = $this->manager_model->get_manager_list();
        $data['manager'] = $manager_list->result();
        
        $this->load->view('admin/manager_list', $data);
        $this->load->view('admin/footer');
    }

    function adding_manager_page()
    {
        $this->general->init_page();
        
        $this->load->view('admin/manager_adding_page');
        $this->load->view('admin/footer');
    }

    function adding_manager()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'password' => $this->input->post('password'),
            'email' => $this->input->post('email'),
        );

        $this->manager_model->add_manager($data);
        redirect('/admin/Manager');
    }

    function manager_editing_page($id="")
    {
        $this->general->init_page();
        
        $manager_one = $this->manager_model->get_manager_one($id);
        $data['manager'] = $manager_one->row();

        $this->load->view('admin/manager_editing_page', $data);
        $this->load->view('admin/footer');
    }

    function editing_manager()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'password' => $this->input->post('password'),
            'email' => $this->input->post('email'),
        );
        $id = $this->input->post('id');

        $this->manager_model->update_manager($data, $id);
        redirect('/admin/Manager/index');
    }

    function delete_manager($id)
    {
        $rs = $this->manager_model->delete_manager($id);
        $data = array('code' => $rs);
        $this->output->set_output(json_encode($data));
    }
}
