<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fold extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Verify_model');
        $this->load->model('key_model');
        $this->load->library('general');
        $this->load->library('FileUpload');
    }

    function editpage($id="")
    {

        $this->general->init_page();
        $key ='fold';
        $fold = $this->key_model->get_fold($key);
        $data['fold'] = $fold->row();
        $this->load->view('admin/fold_edit_page', $data);
        $this->load->view('admin/footer');
    }

    function edit()
    {
        $id = $this->input->post('id');
        $fileSet = array('path' => 'upload/data/', 'file_name' => 'photo1');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['val'] = $upload_info['file_name'];
        }

        $fileSet = array('path' => 'upload/data/', 'file_name' => 'photo2');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['val2'] = $upload_info['file_name'];
        }

        $fileSet = array('path' => 'upload/data/', 'file_name' => 'photo3');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['val3'] = $upload_info['file_name'];
        }

        $fileSet = array('path' => 'upload/data/', 'file_name' => 'photo4');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['val4'] = $upload_info['file_name'];
        }

        $fileSet = array('path' => 'upload/data/', 'file_name' => 'photo5');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(count($data)>0) {
            $mainid = $this->key_model->update_fold($data,$id);
        }
        redirect('/admin/fold/editpage');
    }
    
}
