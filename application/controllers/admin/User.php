<?php
/**
 * 會員
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
    function __construct()
    {
		parent::__construct();
		$this->load->model('Verify_model');
		$this->load->library('general');
		$this->load->model('user_model');
	}
	
    public function index()
    {
		$this->general->init_page(); 
        
		$user_list = $this->user_model->get_user_list('');
		$data['user'] = $user_list->result();
        
		$this->load->view('admin/user_list',$data);
		$this->load->view('admin/footer');
	}
	
    public function edit_user_page($id)
    {
	    $this->general->init_page();
        
        $user_one = $this->user_model->get_user_one($id);
		$data['user'] = $user_one->row();
        
		$this->load->view('admin/user_update_page',$data);	
        $this->load->view('admin/footer');	
	}
	
    public function edit_user()
    {
		$data = array(
		   'name' => $this->input->post('name'),
		   'email' => $this->input->post('email'),
		   'password' => $this->input->post('password'),
		   'gender' => $this->input->post('gender'),
		   'phone' => $this->input->post('phone'),
           'app_id' => $this->input->post('app_id'),
           'created_at' => $this->input->post('created_at'),
           'updated_at' => $this->input->post('updated_at'),
		);

		$id=$this->input->post('id');		
        $this->user_model->update_user($data, $id);

		redirect('/admin/User');
	}
	
    public function delete_user($id)
    {
		$rs = $this->user_model->delete_user($id);
		$data = array(
			'code'=>$rs
        );

		$this->output->set_output(json_encode($data));
	}
}
