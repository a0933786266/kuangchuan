<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Store extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Verify_model');
        $this->load->model('store_model');
        $this->load->model('city_model');
        $this->load->library('general');
        $this->load->library('FileUpload');
        $this->load->model('activity_model');
    }

    function index()
    {
        $this->general->init_page();
        /*
        $data['param']['home'] = $this->input->post('home');
        $data['param']['category_id'] = $this->input->post('category_id');
        $data['param']['tag_1'] = $this->input->post('tag_1');
        $data['param']['tag_3'] = $this->input->post('tag_3');



        $category = $this->store_category_model->get_store_category_list();
        $category_list = $category->result();

        //顯示分類
        $data['store_category'] = [];
        foreach ($category_list as $value) {
            $data['store_category'][$value->id] = $value->title;
        }

        //顯示搜尋標籤
        $tag_one = $this->store_tag_model->get_store_tag_list(['parent_id' => 1]);
        $data['tag_one'] = $tag_one->result();

        $tag_three = $this->store_tag_model->get_store_tag_list(['parent_id' => 3]);
        $data['tag_three'] = $tag_three->result();
        */

        $data['citySelected'] =  $this->input->post('city');
        $data['townSelected'] =  $this->input->post('town');

        $where['city'] =  $this->input->post('city');
        $where['town'] =  $this->input->post('town');

        $citylist = $this->city_model->get_city_list();
        $data['citylist'] = $citylist->result();

        $store = $this->store_model->get_store_list($where);
        $data['store'] = $store->result();

        $this->load->view('admin/store_list.php', $data);
        $this->load->view('admin/footer');
    }

    function adding_store_page()
    {
        $this->general->init_page();
        $citylist = $this->city_model->get_city_list();
        $data['citylist'] = $citylist->result();
        /*
        $category = $this->store_category_model->get_store_category_list();
        $data['category'] = $category->result();

        $tag_one = $this->store_tag_model->get_store_tag_list(['parent_id' => 1]);
        $data['tag_one'] = $tag_one->result();
        $tag_three = $this->store_tag_model->get_store_tag_list(['parent_id' => 3]);
        $data['tag_three'] = $tag_three->result();

        $data['status'] = $this->store_model::STATUS;
        */
        $this->load->view('admin/store_adding_page',$data);
        $this->load->view('admin/footer');
    }


    function edit_store_page($id="")
    {
        $this->general->init_page();

        $citylist = $this->city_model->get_city_list();
        $data['citylist'] = $citylist->result();

        $store = $this->store_model->get_store_one($id);
        //取得單筆的店家資料
        $store_row = $store->row();

        $data['store'] = $store_row;
        /*
        $category = $this->store_category_model->get_store_category_list();
        $data['category'] = $category->result();

        $tag_one = $this->store_tag_model->get_store_tag_list(['parent_id' => 1]);
        $data['tag_one'] = $tag_one->result();

        $tag_three = $this->store_tag_model->get_store_tag_list(['parent_id' => 3]);
        $data['tag_three'] = $tag_three->result();

        $data['status'] = $this->store_model::STATUS;
        */
        $this->load->view('admin/store_editing_page', $data);
        $this->load->view('admin/footer');
    }

    function adding_store()
    {
        $data = array(
            'name' => trim($this->input->post('name')),
            'phone' => $this->input->post('phone'),
            'address' => $this->input->post('address'),
            'bshour' => $this->input->post('bshour'),
            'context' => $this->input->post('context'),
            'city' => $this->input->post('city'),
            'town' => $this->input->post('town'),
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng'),
            'promote' => $this->input->post('promote'),
            'status' => 1
        );

        //上傳圖片
        $fileSet = array('path' => 'upload/store/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }
        $mainid = $this->store_model->add_store($data);
        $this->store_model->update_store($data, $mainid);

        redirect('/admin/store');
    }

    function editing_store()
    {
        $id = $this->input->post('id');
        $data = array(
            'name' => trim($this->input->post('name')),
            'phone' => $this->input->post('phone'),
            'address' => $this->input->post('address'),
            'bshour' => $this->input->post('bshour'),
            'context' => $this->input->post('context'),
            'city' => $this->input->post('city'),
            'town' => $this->input->post('town'),
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng'),
            'promote' => $this->input->post('promote'),
            'status' => $this->input->post('status')
            //'title' => trim($this->input->post('title')),
            //'context' => $this->input->post('context'),
            //'category_id' => implode(',', $this->input->post('category_id')),
            //'tag_1' => $this->input->post('tag_1'),
            // 'tag_2' => $this->input->post('tag_2'),
            //'tag_3' => $this->input->post('tag_3'),
            //'status' => $this->input->post('status'),
            //'link' => $this->input->post('link'),
            //'author' => $this->input->post('author'),
            //'lyric' => $this->input->post('lyric'),
        );

        $fileSet = array('path' => 'upload/store/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }

        $rs = $this->store_model->update_store($data,$id);
        redirect('/admin/store/edit_store_page/'.$id);
    }

    function viewsStore(){
        $this->load->view('admin/viewStore');
    }

    function change_status()
    {
        $id = $this->input->post('id');
        $data = array(
            'status' => $this->input->post('status'),
        );
        $this->store_model->update_store($data, $id);
    }

    function change_home_recommend()
    {
        $id = $this->input->post('id');
        $data = array(
            'home' => $this->input->post('status')
        );
        $this->store_model->update_store($data, $id);

        $data = array(
            'code' => $rs
        );
        $this->output->set_output(json_encode($data));
    }

    function delete_store()
    {
        $where['id'] = $this->input->post('id');
        $where['category_id'] = $this->input->post('category_id');
        $rs = $this->store_model->delete_store($where);
        $data = array('code' => $rs);
        $this->output->set_output(json_encode($data));
    }

    //經典歌曲
    function adding_classic_store()
    {
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
            'category_id' => 6,
            'status' => $this->input->post('status'),
            'link' => $this->input->post('link'),
            'link2' => $this->input->post('link2'),
            'link3' => $this->input->post('link3'),
            'link4' => $this->input->post('link4'),
            'author' => $this->input->post('author'),
            'lyric' => $this->input->post('lyric'),
            'link_title' => $this->input->post('link_title'),
            'link2_title' => $this->input->post('link2_title'),
            'link3_title' => $this->input->post('link3_title'),
            'link4_title' => $this->input->post('link4_title')
        );

        $fileSet = array('path' => 'upload/store/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }

        $mainid = $this->store_model->add_store($data);
        $data = array(
            'rank' => $mainid,
        );
        $this->store_model->update_store($data, $mainid);
        redirect('/admin/store');
    }

    function editing_classic_store(){
        $id = $this->input->post('id');
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
            'status' => $this->input->post('status'),
            'link' => $this->input->post('link'),
            'link2' => $this->input->post('link2'),
            'link3' => $this->input->post('link3'),
            'link4' => $this->input->post('link4'),
            'author' => $this->input->post('author'),
            'lyric' => $this->input->post('lyric'),
            'link_title' => $this->input->post('link_title'),
            'link2_title' => $this->input->post('link2_title'),
            'link3_title' => $this->input->post('link3_title'),
            'link4_title' => $this->input->post('link4_title')
        );

        $fileSet = array('path' => 'upload/store/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }
        $rs = $this->store_model->update_store($data,$id);
        redirect('/admin/store/editing_classic_page/'.$id);
    }


    function exportData(){
        $this->load->library('PHPExcel');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                             ->setLastModifiedBy("Maarten Balliauw")
                             ->setTitle("Office 2007 XLSX Test Document")
                             ->setSubject("Office 2007 XLSX Test Document")
                             ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Test result file");
        $objPHPExcel->setActiveSheetIndex(0);

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );


        // store list
        $store = $this->store_model->listEnable();
        $storeList = $store->result();
        $storeKey = array();
        $rowkey = 3;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "點擊數");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, "日期");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "店名");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, "地址");
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        foreach ($storeList as $key => $row) {
            $storeKey[$row->id] = $rowkey;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $rowkey, $row->name);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $rowkey, $row->address);
            $rowkey++;
        }

        $where['startdate']  = $this->input->post('startdate');
        $where['enddate']  = $this->input->post('enddate');
        $where['mode']  = $this->input->post('mode');
        $record = $this->activity_model->getStoreByDate($where);
        $records = $record->result();

        $col = 1;
        $createdAt= "";
        foreach ($records as $key => $row) {
            if(empty($createdAt) || $createdAt != $row->created_at) {
                $col++;
                $createdAt = $row->created_at;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,1 , $createdAt);
            }
            if (array_key_exists($row->storeid, $storeKey)) {
              $locate = $storeKey[$row->storeid];
              $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $locate, $row->views);
            }
            // if($row->storeid==206){
            //   $locate = $storeKey[$row->storeid];
            //   $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $locate, $row->views);
            // }
        }

        foreach($objPHPExcel->getActiveSheet()->getColumnDimension() as $col) {
            $col->setAutoSize(true);
        }

        // $objPHPExcel->getActiveSheet()->setCellValue('A1','機器');
        // $objPHPExcel->getActiveSheet()->setCellValue('B1','制令');
        // $objPHPExcel->getActiveSheet()->setCellValue('C1',"");
        // $objPHPExcel->getActiveSheet()->setCellValue('D1',"");
        // $objPHPExcel->getActiveSheet()->setCellValue('F1',"");

        // $objPHPExcel->getActiveSheet()->setCellValue('A4','軸序');
        // $objPHPExcel->getActiveSheet()->setCellValue('B4','實際軸長');
        // $objPHPExcel->getActiveSheet()->setCellValue('C4','工單');
        // $objPHPExcel->getActiveSheet()->setCellValue('D4','單長');
        // $objPHPExcel->getActiveSheet()->getStyle('E')->getAlignment()->setWrapText(true);

        // $i=1;
        // foreach($_POST['sorting'] as $sk => $sv){
        //     $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$_POST['sorting'][$sk]);
        //     $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$_POST['onqty'][$sk]);

        //     $workid = str_replace("、","\n",$_POST['workID'][$sk]);
        //     $leng = str_replace("、","\n",$_POST['leng'][$sk]);
        //     $para_leng = str_replace("、","\n",$_POST['para_leng'][$sk]);
        //     $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$workid);
        //     $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$leng);
        //     $i++;
        // }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        // header('Content-Type: application/vnd.ms-excel');
        // header('Content-Disposition: attachment;filename="att_report_' . date('dMy') . '.xls"');
        // header('Cache-Control: max-age=0');

        $filename =date('Y-m-d His').'.xlsx';
        $data_name = 'upload/export/'.$filename;
        $objWriter->save($data_name);
        //exit;
        $res = array(
            'status'=>200,
            'filename'=>$filename
        );
        echo json_encode($res);
    }
}
