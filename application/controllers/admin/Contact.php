<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contact extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Verify_model');
        $this->load->model('contact_model');
        $this->load->library('general');
    }

    function index()
    {
        $this->general->init_page();
        
        $contact = $this->contact_model->get_contact_list();
        $data['contact'] = $contact->result();

        $this->load->view('admin/contact_list.php', $data);
        $this->load->view('admin/footer');
    }

    function contact_edit_page($id="")
    {
        $this->general->init_page();
        
        $contact = $this->contact_model->get_contact_by_id($id);
        $data['contact'] = $contact->row();
        
        $this->load->view('admin/contact_editing_page', $data);
        $this->load->view('admin/footer');
    }

    function adding_contact()
    {
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
            'link' => $this->input->post('link'),
        );
        $this->contact_model->add_contact($data);
        redirect('/admin/contact');
    }
    
    function change_status()
    {
        $id = $this->input->post('id');
        $data = array(
            'status' => $this->input->post('status'),
        );
        $this->contact_model->update_contact($data, $id);
    }
    
    function delete_contact()
    {
        $id = $this->input->post('id');
        $rs = $this->contact_model->delete_contact($id);
        $data = array('code' => $rs);
        $this->output->set_output(json_encode($data));
    }
}
