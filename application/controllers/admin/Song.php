<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Song extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Verify_model');
        $this->load->model('song_model');
        $this->load->model('song_category_model');
        $this->load->model('song_tag_model');
        $this->load->library('general');
        $this->load->library('FileUpload');
    }

    function index()
    {
        $this->general->init_page();
        
        $data['param']['home'] = $this->input->post('home');
        $data['param']['category_id'] = $this->input->post('category_id');
        $data['param']['tag_1'] = $this->input->post('tag_1');
        $data['param']['tag_3'] = $this->input->post('tag_3');

        $song = $this->song_model->get_song_list($data['param']);
        $data['song'] = $song->result();
        
        $category = $this->song_category_model->get_song_category_list();
        $category_list = $category->result();
        
        //顯示分類        
        $data['song_category'] = [];
        foreach ($category_list as $value) {
            $data['song_category'][$value->id] = $value->title;
        }

        //顯示搜尋標籤
        $tag_one = $this->song_tag_model->get_song_tag_list(['parent_id' => 1]);
        $data['tag_one'] = $tag_one->result();

        $tag_three = $this->song_tag_model->get_song_tag_list(['parent_id' => 3]);
        $data['tag_three'] = $tag_three->result();

        $this->load->view('admin/song_list.php', $data);
        $this->load->view('admin/footer');
    }

    function adding_song_page()
    {
        $this->general->init_page();

        $category = $this->song_category_model->get_song_category_list();
        $data['category'] = $category->result();

        $tag_one = $this->song_tag_model->get_song_tag_list(['parent_id' => 1]);
        $data['tag_one'] = $tag_one->result();
        /*
        $tag_two = $this->song_tag_model->get_song_tag_list(['parent_id' => 2]);
        $data['tag_two'] = $tag_two->result();
        */
        $tag_three = $this->song_tag_model->get_song_tag_list(['parent_id' => 3]);
        $data['tag_three'] = $tag_three->result();

        $data['status'] = $this->song_model::STATUS;

        $this->load->view('admin/song_adding_page', $data);
        $this->load->view('admin/footer');
    }

    function adding_classic_page()
    {
        $this->general->init_page();

        $category = $this->song_category_model->get_song_category_list();
        $data['category'] = $category->result();

        $data['status'] = $this->song_model::STATUS;

        $this->load->view('admin/song_add_classic', $data);
        $this->load->view('admin/footer');
    }

    function editing_classic_page($id=""){
        $this->general->init_page();
        
        $song = $this->song_model->get_song_one($id);
        $song_row = $song->row();
        $data['song'] = $song_row;

        $data['status'] = $this->song_model::STATUS;

        $this->load->view('admin/song_edit_classic', $data);
        $this->load->view('admin/footer');
    }

    function edit_song_page($id="")
    {
        $this->general->init_page();
        
        $song = $this->song_model->get_song_one($id);
        $song_row = $song->row();
        $data['song'] = $song_row;

        $category = $this->song_category_model->get_song_category_list();
        $data['category'] = $category->result();

        $tag_one = $this->song_tag_model->get_song_tag_list(['parent_id' => 1]);
        $data['tag_one'] = $tag_one->result();
        /*
        $tag_two = $this->song_tag_model->get_song_tag_list(['parent_id' => 2]);
        $data['tag_two'] = $tag_two->result();
        */
        $tag_three = $this->song_tag_model->get_song_tag_list(['parent_id' => 3]);
        $data['tag_three'] = $tag_three->result();

        $data['status'] = $this->song_model::STATUS;

        $this->load->view('admin/song_editing_page', $data);
        $this->load->view('admin/footer');
    }

    function adding_song()
    {
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
            'category_id' => implode(',', $this->input->post('category_id')),
            'tag_1' => $this->input->post('tag_1'),
            //'tag_2' => $this->input->post('tag_2'),
            'tag_3' => $this->input->post('tag_3'),
            'status' => $this->input->post('status'),
            'link' => $this->input->post('link'),
            'author' => $this->input->post('author'),
            'lyric' => $this->input->post('lyric'),
        );
        /*
        $fileSet = array('path' => 'upload/song/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }
        */
        $mainid = $this->song_model->add_song($data);
        $data = array(
            'rank' => $mainid,
        );
        $this->song_model->update_song($data, $mainid);
        
        redirect('/admin/song');
    }

    function editing_song()
    {
        $id = $this->input->post('id');
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
            'category_id' => implode(',', $this->input->post('category_id')),
            'tag_1' => $this->input->post('tag_1'),
            // 'tag_2' => $this->input->post('tag_2'),
            'tag_3' => $this->input->post('tag_3'),
            'status' => $this->input->post('status'),
            'link' => $this->input->post('link'),
            'author' => $this->input->post('author'),
            'lyric' => $this->input->post('lyric'),
        );
        /*
        $fileSet = array('path' => 'upload/song/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }
        */
        $rs = $this->song_model->update_song($data,$id);
        redirect('/admin/song/edit_song_page/'.$id);
    }
    
    function change_status()
    {
        $id = $this->input->post('id');
        $data = array(
            'status' => $this->input->post('status'),
        );
        $this->song_model->update_song($data, $id);
    }

    function change_home_recommend()
    {
        $id = $this->input->post('id');
        $data = array(
            'home' => $this->input->post('status')
        );
        $this->song_model->update_song($data, $id);

        $data = array(
            'code' => $rs
        );
        $this->output->set_output(json_encode($data));
    }
    
    function delete_song()
    {
        $where['id'] = $this->input->post('id');
        $where['category_id'] = $this->input->post('category_id');
        $rs = $this->song_model->delete_song($where);
        $data = array('code' => $rs);
        $this->output->set_output(json_encode($data));
    }

    //經典歌曲
    function adding_classic_song()
    {
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
            'category_id' => 6,
            'status' => $this->input->post('status'),
            'link' => $this->input->post('link'),
            'link2' => $this->input->post('link2'),
            'link3' => $this->input->post('link3'),
            'link4' => $this->input->post('link4'),
            'author' => $this->input->post('author'),
            'lyric' => $this->input->post('lyric'),
            'link_title' => $this->input->post('link_title'),
            'link2_title' => $this->input->post('link2_title'),
            'link3_title' => $this->input->post('link3_title'),
            'link4_title' => $this->input->post('link4_title')
        );

        $fileSet = array('path' => 'upload/song/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }
        
        $mainid = $this->song_model->add_song($data);
        $data = array(
            'rank' => $mainid,
        );
        $this->song_model->update_song($data, $mainid);
        redirect('/admin/song');
    }

    function editing_classic_song(){
        $id = $this->input->post('id');
        $data = array(
            'title' => trim($this->input->post('title')),
            'context' => $this->input->post('context'),
            'status' => $this->input->post('status'),
            'link' => $this->input->post('link'),
            'link2' => $this->input->post('link2'),
            'link3' => $this->input->post('link3'),
            'link4' => $this->input->post('link4'),
            'author' => $this->input->post('author'),
            'lyric' => $this->input->post('lyric'),
            'link_title' => $this->input->post('link_title'),
            'link2_title' => $this->input->post('link2_title'),
            'link3_title' => $this->input->post('link3_title'),
            'link4_title' => $this->input->post('link4_title')
        );
        
        $fileSet = array('path' => 'upload/song/', 'file_name' => 'image_path');
        $upload_info = $this->fileupload->upload_data($fileSet);

        if(!empty($upload_info['file_name'])) {
            $data['image_path'] = $upload_info['file_name'];
        }
        $rs = $this->song_model->update_song($data,$id);
        redirect('/admin/song/editing_classic_page/'.$id);
    }
}
