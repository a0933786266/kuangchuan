<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>新增歌曲分類</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 首頁</a></li>
            <li><a href="#">歌曲分類管理</a></li>
            <li class="active">新增歌曲分類</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-body pad">
                        <form action="/admin/SongCategory/adding_song_category" id="song_category-form" class="form-horizontal" method="post"  enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="title" class="col-sm-1 control-label">分類名稱</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="title" name="title" placeholder="分類名稱"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="image_path" class="col-sm-1 control-label">圖片(小) 寬600px*高388px</label>
                                <div class="col-sm-6">
                                    <input type="file" name="image_path" class="form-control" placeholder="圖片">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="image_origin" class="col-sm-1 control-label">圖片(大) 1140</label>
                                <div class="col-sm-6">
                                    <input type="file" name="image_origin" class="form-control"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="context" class="col-sm-1 control-label">首頁短介紹</label>
                                <div class="col-sm-10">
                                    <textarea id="context" name="context" rows="10" cols="80"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="longcontext" class="col-sm-1 control-label">分類長介紹</label>
                                <div class="col-sm-10">
                                    <textarea id="longcontext" name="longcontext" rows="10" cols="80"></textarea>
                                </div>
                            </div>

                            <button type="button" id="save" class="btn btn-primary">建立</button>
                            <button type="button" class="btn btn-default" onclick="location.href='/admin/SongCategory'">取消</button>
                        </form>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col-->
        </div><!-- ./row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script src="<?php echo CK;?>ckeditor.js"></script>
<script>
$(function () {
    CKEDITOR.replace('context');
    CKEDITOR.replace('longcontext');
    
    $('#save').click(function(){
        let str="";
        let title = $('#title').val();
        
        if (title === '' ){
            str+="請輸入名稱\n";
        }
        
        if (str !== ''){
            window.alert(str);
            return false;
        } else {
            $('#song_category-form').submit(); 
        }
    })
});
</script>
