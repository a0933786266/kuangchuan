      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            新增管理員
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">管理員</a></li>
            <li class="active">新增管理員</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-body pad">
                  <form action="/admin/Manager/adding_manager" class="form-horizontal" method="post">
                  	<div class="form-group">
                      <label for="title" class="col-sm-1 control-label">姓名</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="name" name="name" placeholder="姓名">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="title" class="col-sm-1 control-label">Email</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="title" class="col-sm-1 control-label">密碼</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="password" name="password" placeholder="密碼">
                      </div>
                    </div>
               
                    <button type="submit" class="btn btn-primary">建立成員</button>
                    <button type="button" class="btn btn-default" onClick="location.href='/admin/Manager'">取消</button>
                  </form>
                </div>
              </div><!-- /.box -->

            </div><!-- /.col-->
          </div><!-- ./row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
	<!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo AdminPlugins ?>bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- CK Editor -->
    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo AdminPlugins ?>bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script>
      $(function () {
        $('#save').click(function(){
            let str="";
            let name = $('#name').val();
            let email = $('#email').val();
            let password = $('#password').val();
            
            if(name === ''){
                str += "請輸入名稱\n";
            }else if(email === ''){
                str+="請輸入信箱\n";
            }else if(password === ''){
                str+="請輸入密碼\n";
            }
            
            if(str !== ''){
                alert(str);
                return false;
            } else {
                $('#company-form').submit(); 
            }
            
        });
      });
    </script>
