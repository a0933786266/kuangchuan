<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>Company管理</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>website管理</a></li>
            <li class="active">Company管理</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped data_table">
                            <thead>
                                <tr>
                                    <th>phone</th>
                                    <th>fax</th>
                                    <th>email</th>
                                    <th>address</th>
                                    <th>編輯</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                foreach ($company as $key => $row) {
                                    echo "<tr>";
                                    echo "<td>".$row->phone."</td>";
                                    echo "<td>".$row->fax."</td>";
                                    echo "<td>".$row->email."</td>";
                                    echo "<td>".$row->address."</td>";
                                    echo "<td><button type='button' class='btn btn-sm btn-success' onclick=location.href='/admin/company/edit_company_page/".$row->id."'>編輯</button></td>";
                                    echo "</tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- DataTables -->
<script src="<?php echo AdminPlugins?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo AdminPlugins?>datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo URL_JS?>/data_table_component.js"></script>
<!-- page script -->
<script language="javascript">
$(function () {
});
</script>
