      <div class="content-wrapper">
        <section class="content-header">
          <h1>新增幻燈片</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 幻燈片</a></li>
            <li><a href="#">幻燈片管理</a></li>
            <li class="active">新增幻燈片</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-body pad">
<form action="/admin/Slide/adding_slide" id="slide-form" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="title" class="col-sm-1 control-label">圖片名稱</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control name" id="title" name="title" placeholder="圖片名稱"/>
                      </div>
                    </div>

                    <div class="form-group">
                        <label for="image_path" class="col-sm-1 control-label">圖片寬825*高313px</label>
                        <div class="col-sm-6">
                            <input type="file" name="image_path" class="form-control" placeholder="圖片">
                        </div>
                    </div>

                    <button type="button" id="save" class="btn btn-primary">建立</button>
                    <button type="button" class="btn btn-default" onclick="location.href='/admin/slide'">取消</button>
                  </form>
                  
                </div>
              </div><!-- /.box -->

            </div><!-- /.col-->
          </div><!-- ./row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
<script>
$(function () {
    
    $('#save').click(function(){
        let str="";
        let title = $('#title').val();
        
        if(title === ''){
            str+="請輸入圖片名稱\n";
        }
        
        if(str!== ''){
            window.alert(str);
            return false;
        } else {
            $('#slide-form').submit(); 
        }
        
    })
  });
</script>
