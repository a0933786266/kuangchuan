<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>編輯歌曲標籤</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>首頁</a></li>
            <li><a href="#">歌曲標籤管理</a></li>
            <li class="active">編輯歌曲標籤</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-body pad">
                        <form action="/admin/SongTag/editing_song_tag" id="home-form" class="form-horizontal" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                              <label for="title" class="col-sm-1 control-label">標題</label>
                              <div class="col-sm-6">
                                <input type="text" class="form-control" id="title" name="title" value="<?php echo $song_tag->title; ?>">
                              </div>
                            </div>
                            <?php if ($song_tag->parent_id != 0) { ?>
                            <div class="form-group">
                                <label for="parent_id" class="col-sm-1 control-label">parent_id</label>
                                <div class="col-sm-6">
                                    <select name="parent_id" class="form-control" id="parent_id">
                                      <?php foreach($song_tag_parent as $key => $value) { ?>
                                      <option value="<?php echo $value->id?>"<?php if ($value->id == $song_tag->parent_id) {echo 'selected';}?>><?php echo $value->title;?></option>
                                      <?php }?>
                                    </select>
                                </div>
                             </div>
                            <?php } ?>
                            <input type="hidden" name="id"  value="<?php echo $song_tag->id; ?>"/>
                            <button type="button" id="save" class="btn btn-primary">儲存</button>
                            <button type="button" class="btn btn-default" onClick="location.href='/admin/SongTag'">取消</button>
                        </form>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col-->
        </div><!-- ./row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- CK Editor -->
<script src="<?php echo CK;?>ckeditor.js"></script>
<script>
$(function () {
    $('#save').click(function(){
        let str = "";
        let title = $('#title').val()
        
        if (title === '' ) {
            str += "請輸入名稱\n";
        }
        
        if (str!== '') {
            alert(str);
            return false;
        } else {
            $('#home-form').submit(); 
        }  
    })
});
</script>
