<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>會員管理</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">會員</a></li>
            <li class="active">會員管理</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped data_table">
                            <thead>
                                <tr>
                                    <th>編號</th>
                                    <th>姓名</th>
                                    <th>信箱</th>
                                    <th>登入次數</th>
                                    <th>上次登入</th>
                                    <th>註冊時間</th>
                                    <!---
                                    <th></th>
                                    <th></th>
                                    -->
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                foreach($user as $key => $row){ 
                                    echo "<tr>";
                                    echo "<td>".$row->id."</td>";
                                    echo "<td>".$row->name."</td>";
                                    echo "<td>".$row->email."</td>";
                                    echo "<td>".$row->views."</td>";
                                    echo "<td>".$row->updated_at."</td>";
                                    echo "<td>".$row->created_at."</td>";
                                  /*
                      echo "<td><button type='button' class='btn btn-success' onclick=location.href='/manager/Users/edit_user_page/".$row->id."'>編輯</button></td>";
                                  echo "<td><button type='button' class='btn btn-danger' onclick=delete_user(".$row->id.")>刪除</button></td>";
                                   */
                                    echo "</tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
      
<!-- DataTables -->
<script src="<?php echo AdminPlugins?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo AdminPlugins?>datatables/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script language="javascript">
$(function(){
    delete_user=function(id){
	    if(confirm("確定刪除此會員嗎?")){
		    $.ajax({
			    method: "POST",
			    url: "/manager/Users/delete_user/"+id,
			    data:{
				    id:id
			    }
		    }).success(function(msg){
			    if(msg!=0){
				    location.reload(); 
				}
		    });
	    } else {
			return false;
		}
	}
});
</script>
