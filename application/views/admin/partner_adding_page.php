<div class="content-wrapper">
    <section class="content-header">
        <h1>新增服務夥伴</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 首頁</a></li>
            <li><a href="#">網站管理</a></li>
            <li class="active">服務夥伴管理</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-body pad">
                        <form action="/admin/Partner/adding_partner"  id="partner-form" class="form-horizontal" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                              <label for="title" class="col-sm-1 control-label">服務夥伴名稱</label>
                              <div class="col-sm-6">
                                <input type="text" class="form-control" id="title" name="title" />
                              </div>
                            </div>
                            <div class="form-group">
                                <label for="company" class="col-sm-1 control-label">夥伴分類</label>
                                <div class="col-sm-6">
                                    <select name="category_id" class="form-control">
                                      <?php foreach($partner_category as $key => $value) { ?>
                                      <option value="<?php echo $value->id?>"><?php echo $value->title?></option>
                                      <?php }?>
                                    </select>
                                </div>
                             </div>

                            <div class="form-group">
                                <label for="image_path" class="col-sm-1 control-label">圖片(600*300)</label>
                                <div class="col-sm-6">
                                    <input type="file" name="image_path" class="form-control" placeholder="圖片">
                                </div>
                            </div>
                     
                            <div class="form-group">
                              <label for="link" class="col-sm-1 control-label">網址</label>
                              <div class="col-sm-6">
                                <input type="text" class="form-control" name="link"/>
                              </div>
                            </div>
                             
                            <div class="form-group">
                                <label for="context" class="col-sm-1 control-label">服務夥伴介紹</label>
                                <div class="col-sm-10">
                                    <textarea id="context" name="context" rows="10" cols="80"></textarea>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">確認</button>
                            <button type="button" class="btn btn-default" onClick="location.href='/admin/Partner'">取消</button>
                        </form>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col-->
        </div><!-- ./row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script src="<?php echo CK;?>ckeditor.js"></script>
<script>
    $(function () {
        CKEDITOR.replace('context');

        $('#save').click(function(){
            let str="";
            let title = $('#title').val();
            
            if(title === ''){
                str+="請輸入名稱\n";
            }
           
            if(str !== ''){
                alert(str);
                return false;
            }else{
                $('#partner-form').submit(); 
            }
        });
});
</script>
