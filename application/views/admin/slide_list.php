<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>幻燈片管理</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>首頁</a></li>
            <li class="active">幻燈片管理</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped data_table">
                        <caption>
                            <a class="btn btn-primary pull-left" href="/admin/Slide/adding_slide_page">新增首頁幻燈片</a>
                        </caption>
                            <thead>
                                <tr>
                                    <th>編號</th>
                                    <th>排序</th>
                                    <th>圖片</th>
                                    <th>圖片文字</th>
                                    <th>編輯</th>
                                    <th>刪除</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                foreach ($slide as $key => $row) {
                                    echo "<tr>";
                                    echo "<td>".$row->id."</td>";
                                    echo "<td><input type='number' class='ranking' value='".$row->rank."' id='".$row->id."' style='width:50px;'/></td>";
                                    echo "<td>".$row->image_path."</td>";
                                    echo "<td>".$row->title."</td>";
                                    echo "<td><button type='button' class='btn btn-sm btn-success' onclick=location.href='/admin/slide/edit_slide_page/".$row->id."'>編輯</button></td>";
                                    echo "<td><button type='button' class='btn btn-sm btn-danger' onclick=delete_slide('".$row->id."')>刪除</button></td>";
                                    echo "</tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- DataTables -->
<script src="<?php echo AdminPlugins?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo AdminPlugins?>datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo URL_JS?>/data_table_component.js"></script>
<!-- page script -->
<script language="javascript">
$(function () {
    delete_slide=function(id){
        if (confirm("確定刪除此頁面嗎?")) {
            $.ajax({
                method: "POST",
  			    url: "/admin/Slide/delete_slide/",
  			    data:{
  				    id:id
  			    }
  		    }).success(function(msg){
  		        if(msg!=0){
                    location.reload(); 
  			    }
  		    });
        } else {
		    return false;
	    }
    }
    
    $(document.body).on("keyup",".ranking",function(){
        let id=$(this).attr("id");
        $.ajax({
            method: "POST",
            url: "/admin/Slide/change_ranking",
            data:{
                id:id,
                rank:$(this).val()
            }
        }).success(function(res){
            alert('更新成功')
        });
    });
});
</script>
