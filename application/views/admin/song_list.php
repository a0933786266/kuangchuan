<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>歌曲管理</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>歌曲管理</a></li>
            <li class="active">歌曲管理</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">              
        <form method="post">
        <ul class="list-unstyled list-inline ">
            <li>
                <a class="btn btn-primary" href="/admin/Song/adding_song_page">新增歌曲</a>
            </li>
            <li>
                <a class="btn btn-success" href="/admin/Song/adding_classic_page">新增經典歌曲</a>
            </li>
            <li>
                <label>首頁推薦</label>
                <select name="home" class="form-control input-sm">
                <option value="All">全部</option>
                <option value="0" <?php echo ($param['home']=='0') ? 'selected': ''?>>非首頁推薦</option>
                    <option value="1" <?php echo ($param['home']=='1') ? 'selected': ''?>>首頁推薦</option>
                </select>
            </li>
            <li>
                <label>分類</label>
                <select name="category_id" class="form-control input-sm" id="filter_category">
                    <option value="All">全部</option>
                    <?php foreach($song_category as $key => $value) {?>
                    <option value="<?php echo $key;?>" <?php if (isset($param['category_id']) && $key == $param['category_id']) { echo "selected"; } ?> ><?php echo $value;?></option>
                    <?php } ?>
                </select>
            </li>
            <li>
                <label>語文</label>
                <select name="tag_1" class="form-control input-sm" id="filter_tag_one">
                    <option value="">請選擇</option>
                    <?php foreach($tag_one as $key => $value) {?>
                    <option value="<?php echo $value->id;?>" <?php if (isset($param['tag_1']) && $value->id == $param['tag_1']) { echo "selected"; } ?> ><?php echo $value->title;?></option>
                    <?php } ?>
                </select>
            </li>
            <li>
                <label>節奏</label>
                <select name="tag_3" class="form-control input-sm" id="filter_tag_three">
                    <option value="">請選擇</option>
                    <?php foreach($tag_three as $key => $value) {?>
                    <option value="<?php echo $value->id;?>" <?php if (isset($param['tag_3']) && $value->id == $param['tag_3']) { echo "selected"; } ?>><?php echo $value->title;?></option>
                    <?php } ?>
                </select>
            </li>
            <li>
                <button type="submit" class="btn btn-success ">查詢</button>
            </li>
        </ul>
        </form>    
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped data_table">
                            
                            <thead>
                                <tr>
                                    <th>編號</th>
                                    <!--<th>排序</th>-->
                                    <th>首頁推薦</th>
                                    <!--<th>瀏覽次數</th>-->
                                    <th>分類</th>
                                    <th>歌名</th>
                                    <th>歌者</th>
                                    <th>刊登狀態</th>
                                    <th>編輯</th>
                                    <th>刪除</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                foreach ($song as $key => $row) {
                                    echo "<tr>";
                                    echo "<td>".$row->id."</td>";
                                    // echo "<td>".$row->views."</td>";
                                    echo "<td>";
                                    echo "<select id=".$row->id." class='home'>";
                                    if($row->home==1){
                                        echo "<option value='1' selected>加入</option>";
                                        echo "<option value='0' >不加入</option>";
                                    } else {
                                        echo "<option value='1' >加入</option>";
                                        echo "<option value='0' selected>不加入</option>";
                                    }
                                    
                                    echo "</select>";
                                    echo "</td>";
                                    echo "<td>".$row->category_name."</td>";
                                    //echo "<td><input type='number' class='ranking' value='".$row->rank."' id='".$row->id."' style='width:50px;'/></td>";
                                    echo "<td>".$row->title."</td>";
                                    echo "<td>".$row->author."</td>";

                                    if($row->status==1){
                                        echo "<td><i class='glyphicon glyphicon-ok'></i></td>";
                                    } else {
                                        echo "<td><i class='glyphicon glyphicon-remove'></i></td>";
                                    }

                                    if($row->category_id==6){
                                        echo "<td><button type='button' class='btn btn-sm btn-success' onclick=location.href='/admin/song/editing_classic_page/".$row->id."'>編輯</button></td>";
                                    } else {
                                        echo "<td><button type='button' class='btn btn-sm btn-success' onclick=location.href='/admin/song/edit_song_page/".$row->id."'>編輯</button></td>";
                                    }
                                    
                                    echo "<td><button type='button' class='btn btn-sm btn-danger' onclick=delete_song(".$row->id.",".$row->category_id.")>刪除</button></td>";
                                    echo "</tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- DataTables -->
<script src="<?php echo AdminPlugins?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo AdminPlugins?>datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo URL_JS?>/data_table_component.js"></script>
<!-- page script -->
<script language="javascript">
$(function () {
    delete_song=function(id, category_id){
        if (confirm("確定刪除此Song嗎?")) {
            $.ajax({
                method: "POST",
  			    url: "/admin/Song/delete_song/",
  			    data:{
  				    id:id,
                    category_id: category_id
  			    }
  		    }).success(function(msg){
  		        if(msg!=0){
                    location.reload(); 
  			    }
  		    });
        } else {
		    return false;
	    }
    }
    
    $(document.body).on("keyup",".ranking",function(){
        let id=$(this).attr("id");
        $.ajax({
            method: "POST",
            url: "/admin/Song/change_ranking",
            data:{
              id: id,
              rank: $(this).val()
            }
        }).success(function(res){
            window.alert('更新成功')
        })
    })

    $(document.body).on("change",".home",function(){
        let id=$(this).attr("id");
        $.ajax({
            method: "POST",
            url: "/admin/Song/change_home_recommend",
            data:{
                id:id,
                status:$(this).val()
            }
        }).success(function(res){
            window.alert('更新成功')
        });
    });
});
</script>
