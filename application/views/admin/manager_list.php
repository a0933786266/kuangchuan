<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>管理者</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 首頁</a></li>
            <li class="active">管理者</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped data_table">
                        <caption>
                            <a class="btn btn-primary pull-left" href="/admin/manager/adding_manager_page">新增管理員</a>
                        </caption>
                            <thead>
                                <tr>
                                    <th>編號</th>
                                    <th>姓名</th>
                                    <th>信箱</th>
                                    <th>編輯</th>
                                    <th>刪除</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                foreach ($manager as $key => $row) {
                                    echo "<tr>";
                                    echo "<td>" . $row->id . "</td>";
                                    echo "<td>" . $row->name . "</td>";
                                    echo "<td>" . $row->email . "</td>";
                                    echo "<td><button type='button' class='btn btn-success' onclick=location.href='/admin/Manager/manager_editing_page/" .
                                        $row->id . "'>編輯</button></td>";
                                    echo "<td><button type='button' class='btn btn-danger' onclick=delete_manager(" .
                                        $row->id . ")>刪除</button></td>";
                                    echo "</tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper --> 
<script src="<?php echo AdminPlugins ?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo AdminPlugins ?>datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo URL_JS?>/data_table_component.js"></script>
<script language="javascript">
$(function(){
    delete_manager = function(id){
		if(confirm("確定刪除管理員嗎?")){
		    $.ajax({
			    method: "POST",
			    url: "/admin/Manager/delete_manager/"+id,
			    data:{
				    id:id
			    }
		    }).success(function(msg){
				if(msg.code!=""){
				    alert('該manager已刪除');
                    location.reload(); 
				}
		    });
		} else {
			return false;
		}
	}
	
    $("#datatable").DataTable();
});
</script>
