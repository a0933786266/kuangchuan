      <div class="content-wrapper">
        <section class="content-header">
          <h1>編輯產品</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">產品管理</a></li>
            <li class="active">編輯產品</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-body pad">
                  <form action="/admin/Product/editing_product" id="product-form" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="name" class="col-sm-1 control-label">品名</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $product->name; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="price" class="col-sm-1 control-label">價格</label>
                      <div class="col-sm-6">
                        <input type="number" class="form-control" id="price" name="price" value="<?php echo $product->price; ?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="cid" class="col-sm-1 control-label">廠商</label>
                        <div class="col-sm-6">
                            <select name="cid" class="form-control" id="cid">
                              <?php foreach($company as $key => $value) { ?>
                              <option value="<?php echo $value->id?>"<?php if ($product->cid === $value->id) {echo 'selected';}?>><?php echo$value->name?></option>
                              <?php }?>
                            </select>
                        </div>
                     </div>

                     <div class="form-group">
                        <label for="status" class="col-sm-1 control-label">狀態</label>
                        <div class="col-sm-6">
                            <select name="status" class="form-control" id="status">
                              <option value="0" <?php if (0 == $product->status){ echo 'selected';}?>>截止</option>
                              <option value="1" <?php if (1 == $product->status){ echo 'selected';}?>>開放中</option>
                            </select>
                        </div>
                    </div>
                             
                    <!---
                    <div class="form-group">
                        <label for="picture" class="col-sm-1 control-label">產品圖片</label>
                        <div class="col-sm-6">
                            <input type="file" name="picture" class="form-control" id="picture" />
                            <img src="<?php echo Upload . "products/" . $product->picture ?>" width="50%" alt="<?php echo $product->picture; ?>"/>
                        </div>
                    </div>
                    -->

                    <div class="form-group">
                        <label for="file" class="col-sm-1 control-label">圖片</label>
                        <div class="col-sm-10">
                            <!--<input type="file" name="file[]" class="form-control file" multiple>-->
                            <input id="input-ke-1" name="file[]" type="file" multiple class="file-loading" accept="image" data-priview='<?php echo $images['preview'];?>' data-config='<?php echo $images['config'];?>'>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="content" class="col-sm-1 control-label">產品介紹</label>
                        <div class="col-sm-10">
                            <textarea id="content" name="content" rows="10" cols="80"><?php echo $product->content; ?></textarea>
                        </div>
                    </div> 
                    <input type="hidden" name="id"  value="<?php echo $product->id; ?>"/>
                    
                    <button type="button" id="save" class="btn btn-primary">儲存產品</button>
                    <button type="button" class="btn btn-default" onClick="location.href='/admin/product'">取消</button>
                  </form>
                </div>
              </div><!-- /.box -->

            </div><!-- /.col-->
          </div><!-- ./row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
    <!-- CK Editor -->
<style type="text/css">
.label_width{
    width:60px;
    padding:0px;
}
</style>    
<script src="<?php echo CK;?>ckeditor.js"></script>
<script>
$(function () {
    CKEDITOR.replace('content');
    
    
    $('#save').click(function(){
        let str = "";
        let name = $('.name').eq(0).val();
        let price = $('.price').eq(0).val();
        
        if(name==""){
            str+="請輸入名稱\n";
        } else if(price==""){
            str+="請輸入價格\n";
        }
        
        if (str!="") {
            alert(str);
            return false;
        } else {
          console.log(111);
            $('#product-form').submit(); 
        }  
    })

    let priview = $("#input-ke-1").data("priview");
    let config = $("#input-ke-1").data("config");
    let pid = $("input[name=id]").val();

    $("#input-ke-1").fileinput({
        theme: "explorer",
        uploadUrl: "/admin/product/add_file/" + pid,
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        overwriteInitial: false,
        initialPreviewAsData: true,
        initialPreview: priview,
        initialPreviewConfig: config
    });
});


</script>
