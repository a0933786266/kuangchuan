      <div class="content-wrapper">
        <section class="content-header">
          <h1>編輯乳香世家故事</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">故事編輯</a></li>
            <li class="active">故事編輯</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-body pad">
                  <form action="/admin/Story/edit" id="home-form" class="form-horizontal" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                      <label for="name" class="col-sm-1 control-label">手機版本圖片(410*133)</label>
                      <div class="col-sm-6">
                        <input type="file" class="form-control" name="mobile"/>
                        <img src="/upload/data/<?php echo $story->val; ?>" width="300"/>
                      </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="cid" class="col-sm-1 control-label">電腦版本圖片(1920*370)</label>
                        <div class="col-sm-6">
                            <input type="file" class="form-control" name="desktop"/">
                            <img src="/upload/data/<?php echo $story->val2; ?>" width="300"/>
                        </div>
                     </div>

                    
                    <input type="hidden" name="id"  value="<?php echo $story->id; ?>"/>

                    <button type="submit" id="save" class="btn btn-primary">儲存</button>
                    <button type="button" class="btn btn-default" onClick="location.href='/admin/Story/editpage'">取消</button>
                  </form>
                </div>
              </div><!-- /.box -->

            </div><!-- /.col-->
          </div><!-- ./row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
