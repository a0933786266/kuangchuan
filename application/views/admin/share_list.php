<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>歌曲分享管理</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>管理</a></li>
            <li class="active">歌曲分享管理</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped data_table">
                        <caption>
                        </caption>
                            <thead>
                                <tr>
                                    <th>編號</th>
                                    <th>首頁推薦</th>
                                    <th>使用歌曲</th>
                                    <th>分享者姓名</th>
                                    <th>歌曲名稱</th>
                                    <th>內容</th>
                                    <th>狀態</th>
                                    <th>刪除</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                foreach ($share as $key => $row) {
                                    echo "<tr>";
                                    echo "<td>".$row->id."</td>";
                                    echo "<td>";
                                    echo "<select data-id=".$row->id." class='home'>";
                                    if($row->home==1){
                                        echo "<option value='1' selected>加入</option>";
                                        echo "<option value='0' >不加入</option>";
                                    } else {
                                        echo "<option value='1' >加入</option>";
                                        echo "<option value='0' selected>不加入</option>";
                                    }
                                    echo "</select>";
                                    echo "</td>";
                                    echo "<td>".$row->title."</td>";
                                    
                                    echo "<td>".$row->name."</td>";
                                    echo "<td>".$row->link."</td>";
                                    echo "<td>".$row->context."</td>";
                                    //echo "<td><button type='button' class='btn btn-sm btn-success' onclick=location.href='/admin/share/share_edit_page/".$row->id."'>編輯</button></td>";
                                    echo "<td>";
                                    echo "<select id=".$row->id." class='status'>";
                                    if($row->status==1){
                                        echo "<option value='1' selected>顯示</option>";
                                        echo "<option value='0' >不顯示</option>";
                                    } else {
                                        echo "<option value='1' >顯示</option>";
                                        echo "<option value='0' selected>不顯示</option>";
                                    }
                                    echo "</select>";
                                    echo "</td>";

                                    echo "<td><button type='button' class='btn btn-sm btn-danger' onclick=delete_share('".$row->id."')>刪除</button></td>";
                                    echo "</tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- DataTables -->
<script src="<?php echo AdminPlugins?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo AdminPlugins?>datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo URL_JS?>/data_table_component.js"></script>
<!-- page script -->
<script language="javascript">
$(function () {
    delete_share = function(id){
        if (confirm("確定刪除此頁面嗎?")) {
            $.ajax({
                method: "POST",
  			    url: "/admin/Share/delete_share/",
  			    data:{
  				  id:id
  			    }
  		    }).success(function(res){
                let data = jQuery.parseJSON(res);
  		        if(data.status !== 200){
                    location.reload(); 
  			    }
  		    });
        } else {
		    return false;
	    }
    }

    $(document.body).on("change",".status",function(){
        let id=$(this).attr("id");
        $.ajax({
            method: "POST",
            url: "/admin/Share/change_status",
            data:{
              id: id,
              status: $(this).val()
            }
        }).success(function(res){
            window.alert('更新成功')
        })
    })

    $('.home').change(function(){
        let id = $(this).data("id");
        $.ajax({
            method: "POST",
            url: "/admin/Share/change_home_recommend",
            data:{
                id:id,
                status:$(this).val()
            }
        }).success(function(res){
            alert('更新成功')
        });
    });
});
</script>
