<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>歌曲分類管理</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>歌曲分類管理</a></li>
            <li class="active">歌曲分類管理</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped data_table">
                            <caption>
                                <a class="btn btn-primary pull-left" href="/admin/SongCategory/adding_song_category_page">新增歌曲分類</a>
                            </caption>
                            <thead>
                                <tr>
                                    <th>編號</th>
<!--<th>排序</th>-->
                                    <th>分類名稱</th>
                                    <th>歌曲數量</th>
                                    <th>圖片</th>
                                    <th>編輯</th>
                                    <th>刪除</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                foreach ($song_category as $key => $row) {
                                    echo "<tr>";
                                    echo "<td>".$row->id."</td>";
                                    //echo "<td><input type='number' class='ranking' value='".$row->rank."' id='".$row->id."' style='width:50px;'/></td>";
                                    echo "<td>".$row->title."</td>";
                                    echo "<td>".$row->qty."</td>";
                                    if (!empty($row->image_path)) {
                                        echo "<td><image width='30%' src='/upload/song/".$row->image_path."'></td>";
                                    } else {
                                        echo "<td></td>";
                                    }
                                    echo "<td><button type='button' class='btn btn-sm btn-success' onclick=location.href='/admin/SongCategory/edit_song_category_page/".$row->id."'>編輯</button></td>";
                                    echo "<td><button type='button' class='btn btn-sm btn-danger' onclick=delete_song_category('".$row->id."')>刪除</button></td>";
                                    echo "</tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- DataTables -->
<script src="<?php echo AdminPlugins?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo AdminPlugins?>datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo URL_JS?>/data_table_component.js"></script>
<!-- page script -->
<script language="javascript">
$(function () {
    delete_song_category=function(id){
        if (confirm("確定刪除此Category嗎?")) {
            $.ajax({
                method: "POST",
  			    url: "/admin/SongCategory/delete_song_category/",
  			    data:{
  				    id:id
  			    }
  		    }).success(function(msg){
  		        if(msg!=0){
                    location.reload(); 
  			    }
  		    });
        } else {
		    return false;
	    }
    }
    
    $(document.body).on("keyup",".ranking",function(){
        let id=$(this).attr("id");
        $.ajax({
            method: "POST",
            url: "/admin/SongCategory/change_ranking",
            data:{
                id:id,
                rank:$(this).val()
            }
        }).success(function(res){
            alert('Update Successfully')
        });
    });
});
</script>
