      <div class="content-wrapper">
        <section class="content-header">
          <h1>新增產品</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 首頁</a></li>
            <li><a href="#">產品管理</a></li>
            <li class="active">新增產品</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-body pad">
                  <form action="/admin/Product/adding_product" id="product-form" class="form-horizontal" method="post"  enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="name" class="col-sm-1 control-label">品名</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control name" id="name" name="name" placeholder="品名"/>
                      </div>
                    </div>

                    <div class="form-group">
                        <label for="price" class="col-sm-1 control-label">價格</label>
                        <div class="col-sm-6">
                            <input type="text" name="price" class="form-control price" id="price"/>
                        </div>
                     </div>

                    <div class="form-group">
                        <label for="company" class="col-sm-1 control-label">廠商</label>
                        <div class="col-sm-6">
                            <select name="cid" class="form-control" id="company">
                              <?php foreach($company as $key => $value) { ?>
                              <option value="<?php echo $value->id?>"><?php echo $value->name?></option>
                              <?php }?>
                            </select>
                        </div>
                     </div>

                    <div class="form-group">
                        <label for="status" class="col-sm-1 control-label">狀態</label>
                        <div class="col-sm-6">
                            <select name="status" class="form-control" id="status">
                              <option value="0">截止</option>
                              <option value="1" selected>開放中</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="file" class="col-sm-1 control-label">圖片</label>
                        <div class="col-sm-10">
                            <input id="input-ke-1" name="file[]" type="file" multiple class="file-loading" accept="image" data-priview='' data-config=''>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="content" class="col-sm-1 control-label">產品介紹</label>
                        <div class="col-sm-10">
                            <textarea id="content" name="content" rows="10" cols="80"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="file_id" value="" id="file_id">
                    <button type="button" id="save" class="btn btn-primary">建立</button>
                    <button type="button" class="btn btn-default" onclick="location.href='/admin/product'">取消</button>
                  </form>
                  
                </div>
              </div><!-- /.box -->

            </div><!-- /.col-->
          </div><!-- ./row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
<style type="text/css">
.label_width{
    width:60px;
    padding:0px;
}
</style>
<script src="<?php echo CK;?>ckeditor.js"></script>
<script>
$(function () {
    CKEDITOR.replace('content')
    
    $('#save').click(function(){
        let str="";
        let name=$('#name').val()
        let price=$('#price').val()
        
        if(name === ''){
            str+="請輸入名稱\n";
        } else if(price==""){
            str+="請輸入價格\n";
        }
        
        if(str!=""){
            window.alert(str);
            return false;
        } else {
            $('#product-form').submit(); 
        }
        
    })

    let priview = $("#input-ke-1").data("priview");
    let config = $("#input-ke-1").data("config");

    $("#input-ke-1").fileinput({
        theme: "explorer",
        uploadUrl: "/admin/product/add_file/",
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        overwriteInitial: false,
        initialPreviewAsData: true,
        initialPreview: priview,
        initialPreviewConfig: config
    })

    let file_ids = [];

    // 新增
    $('#input-ke-1').on('fileuploaded', function(event, data, previewId, index) {
        let form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        
        if (response.id !== null) {
            file_ids.push(response.id);
            $('#' + previewId).attr('data-id', response.id);
            $('#file_id').val(file_ids);
        }
    });

    // 刪除
    $('#input-ke-1').on('filesuccessremove', function(event, id) {
        let file_id = $('#' + id).data('id');
        let index = file_ids.indexOf(file_id);

        delete file_ids[index];
        $('#file_id').val(file_ids);
    });

  });
</script>
