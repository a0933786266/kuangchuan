      <div class="content-wrapper">
        <section class="content-header">
          <h1>編輯熱門話題</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> hot</a></li>
            <li><a href="#">熱門話題管理</a></li>
            <li class="active">編輯熱門話題</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-body pad">
                  <form action="/admin/Hot/editing_hot" id="hot-form" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="title" class="col-sm-1 control-label">主題</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="title" name="title" value="<?php echo $hot->title; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="link" class="col-sm-1 control-label">連結</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" name="link" value="<?php echo $hot->link; ?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="image_path" class="col-sm-1 control-label">圖片</label>
                        <div class="col-sm-6">
                            <input type="file" name="image_path" class="form-control" placeholder="圖片">
                            <img src="<?php echo '/upload/hot/'.$hot->image_path ?>" width="50%"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="context" class="col-sm-1 control-label">介紹</label>
                        <div class="col-sm-10">
                            <textarea id="context" name="context" rows="10" cols="80"><?php echo $hot->context; ?></textarea>
                        </div>
                    </div> 
                    <input type="hidden" name="id"  value="<?php echo $hot->id; ?>"/>
                    
                    <button type="button" id="save" class="btn btn-primary">儲存</button>
                    <button type="button" class="btn btn-default" onClick="location.href='/admin/Hot'">取消</button>
                  </form>
                </div>
              </div><!-- /.box -->

            </div><!-- /.col-->
          </div><!-- ./row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
    <!-- CK Editor -->  
<script src="<?php echo CK;?>ckeditor.js"></script>
<script>
$(function () {
    CKEDITOR.replace('context');
    
    $('#save').click(function(){
        let str = "";
        let title = $('#title').val();
        
        if(title === ''){
            str += "請輸入主題\n";
        }

        if (str !== '') {
            alert(str);
            return false;
        } else {
            $('#hot-form').submit(); 
        }  
    })
});


</script>
