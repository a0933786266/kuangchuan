<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>服務夥伴管理</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 首頁</a></li>
            <li class="active">服務夥伴管理</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="datatable" class="table table-bordered table-striped">
                        <caption>
                            <a class="btn btn-primary pull-left" href="/admin/Partner/adding_partner_page">新增夥伴</a>
                        </caption>
                            <thead>
                                <tr>
                                    <th>編號</th>
                                    <th>夥伴名稱</th>
                                    <th>夥伴分類</th>
                                    <th>連結</th>
                                    <th>建立時間</th>
                                    <th>編輯</th>
                                    <th>刪除</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                foreach($partner as $key => $row){ 
                                    echo "<tr>";
                                    echo "<td>".$row->id."</td>";
                                    echo "<td>".$row->title."</td>";
                                    echo "<td>".$row->category_name."</td>";
                                    echo "<td>".$row->link."</td>";
                                    echo "<td>".$row->created_at."</td>";
                                    echo "<td><button type='button' class='btn btn-success' onclick=location.href='/admin/Partner/edit_partner_page/".$row->id."'>編輯</button></td>";
                                    echo "<td><button type='button' class='btn btn-danger' onclick=delete_partner('".$row->id."')>刪除</button></td>";
                                    echo "</tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- DataTables -->
<script src="<?php echo AdminPlugins?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo AdminPlugins?>datatables/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script language="javascript">
$(function(){
    delete_partner=function(id){
        if (confirm("確定刪除此Partner嗎?")) {
            $.ajax({
                method: "POST",
                url: "/admin/Partner/delete_partner/",
                data:{
                    id:id
                }
            }).success(function(msg){
                if(msg!=0){
                    location.reload(); 
                }
            });
        } else {
            return false;
        }
    }

    $("#datatable").DataTable();
});
</script>
