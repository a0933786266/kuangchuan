<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>編輯店家資訊</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 店家</a></li>
            <li><a href="#">店家管理</a></li>
            <li class="active">編輯店家</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-body pad">
                        <form action="/admin/Store/editing_store" id="store-form" class="form-horizontal" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="title" class="col-sm-1 control-label">店名</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="name" name="name" value="<?php echo $store->name; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="image_path" class="col-sm-1 control-label">店家圖片</label>
                                <div class="col-sm-6">
                                    <input type="file" name="image_path" class="form-control" placeholder="店家圖片">圖片尺寸(300*225)
                                    <img src="<?php echo '/upload/store/'.$store->image_path ?>" width="50%"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title" class="col-sm-1 control-label">電話</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $store->phone; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="tag_1" class="col-sm-1 control-label">縣市</label>
                                <div class="col-sm-3">
                                    <select name="city" class="city form-control">
                                        <?php 
                                            foreach($citylist as $key => $value) { 
                                                if($store->city == $value->id){
                                                    echo "<option selected value='". $value->id."' title='".$value->road_name."'>".$value->name."</option>";
                                                } else {
                                                    echo "<option value='". $value->id."' title='".$value->road_name."'>".$value->name."</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select name="town" class="town form-control"></select>
                                    <input type="hidden" id="townid" value="<?php echo $store->town;?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="title" class="col-sm-1 control-label">地址</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="address" name="address" value="<?php echo $store->address; ?>">
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" class="convert">轉換經緯度</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="title" class="col-sm-1 control-label">經度</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="lat" name="lat" value="<?php echo $store->lat; ?>">
                                </div>
                                <label for="title" class="col-sm-1 control-label">緯度</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="lng" name="lng" value="<?php echo $store->lng; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title" class="col-sm-1 control-label">營業時間</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="bshour" name="bshour" value="<?php echo $store->bshour; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title" class="col-sm-1 control-label">狀態</label>
                                <div class="col-sm-6">
                                    <select name="status" class="form-control">
                                        <option value="1" <?php echo ($store->status==1) ? 'selected':''?>>啟用</option>
                                        <option value="0" <?php echo ($store->status==0) ? 'selected':''?>>關閉</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title" class="col-sm-1 control-label">推薦</label>
                                <div class="col-sm-6">
                                    <select name="promote" class="form-control">
                                        <option value="0" <?php echo ($store->promote==0) ? 'selected':''?>>一般店家</option>
                                        <option value="1" <?php echo ($store->promote==1) ? 'selected':''?>>特別推薦</option>
                                    </select>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <label for="context" class="col-sm-1 control-label">簡介</label>
                                <div class="col-sm-10">
                                    <textarea id="context" name="context" rows="10" cols="80">
                                        <?php echo $store->context;?>
                                    </textarea>
                                </div>
                            </div>

                            <input type="hidden" name="id"  value="<?php echo $store->id; ?>"/>
                            <button type="button" id="save" class="btn btn-primary">儲存</button>
                            <button type="button" class="btn btn-default" onClick="location.href='/admin/store'">取消</button>
                        </form>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col-->
        </div><!-- ./row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- CK Editor -->
<script src="<?php echo CK;?>ckeditor.js"></script>
<script type="text/javascript"  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0awVbvNlfrp5hABP_jmCtWrRLq40H8iM&callback=initMap" async defer></script>
<script>
$(function () {
    CKEDITOR.replace('context');
    
    $('.city').change(function(){
        let townid = parseInt($('#townid').val())

        let townList = $('.city option:selected').attr("title")
            townList = townList.split("|")
            townList.shift()

        let optionStr = ''
        $.each(townList,function(idx,val){
            idx+=1
            if (townid === idx) {
                optionStr += "<option selected value='" + idx + "'>" + val + "</option>";
            } else {
                optionStr += "<option value='" + idx + "'>" + val + "</option>";
            }
        })
        $(".town").html(optionStr)
    }).trigger('change')

    $('#save').click(function(){
        let str = "";
        let name = $('#name').val()
        let phone = $('#phone').val()
        let address = $('#address').val()

        if (name === '') {
            str += "請輸入店名稱\n";
        } else if (phone === '') {
            str += "請填寫電話\n";
        } else if (address === '') {
            str += "請填寫地址\n";
        }

        if (str !== '') {
            window.alert(str)
            return false
        } else {
            $('#store-form').submit() 
        }  
    })

    $(".convert").click(function(){   
        let address = $("#address").val();
        if(address === ''){
            alert('請先輸入地址')
            return false
        }
        getLatLngByAddr(address);
    });

    //地址轉經緯度
    getLatLngByAddr = function (address) {   
        var geocoder = new google.maps.Geocoder();  //定義一個Geocoder物件   
        geocoder.geocode({ address: address},       //設定地址的字串   
            function(results, status) {             //callback function   
                if (status == google.maps.GeocoderStatus.OK) {    //判斷狀態   
                    // alert(results[0].geometry.location);
                    //取得座標
                    let lat = results[0].geometry.location.lat();   
                    let lng = results[0].geometry.location.lng();
                    console.log(lat + ', ' +lng)
                    let LatLng = results[0].geometry.location
                    $('#lat').val(lat)
                    $('#lng').val(lng)                         
                } else {   
                    alert('Error');   
                }   
          }   
        );
    }
});
</script>
