<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>編輯歌曲</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 歌曲</a></li>
            <li><a href="#">編輯歌曲管理</a></li>
            <li class="active">編輯歌曲</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-body pad">
                        <form action="/admin/Song/editing_classic_song" id="song-form" class="form-horizontal" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="title" class="col-sm-1 control-label">主題名稱</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="title" name="title" value="<?php echo $song->title; ?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="image_path" class="col-sm-1 control-label">主題圖片</label>
                                <div class="col-sm-6">
                                    <input type="file" name="image_path" class="form-control" placeholder="圖片">
                                    <img src="<?php echo '/upload/song/'.$song->image_path ?>" width="50%" alt="<?php echo $song->image_path; ?>"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="category_id" class="col-sm-1 control-label">歌曲分類</label>
                                <div class="col-sm-6">
                                經典歌曲
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <label for="status" class="col-sm-1 control-label">狀態</label>
                                <div class="col-sm-6">
                                    <select name="status" class="form-control" id="status">
                                        <?php 
                                        foreach($status as $key => $value) { 
                                            if($song->status == $key){
                                                echo '<option value='.$key.' selected>'.$value.'</option>';
                                            } else {
                                                echo '<option value='.$key.'>'.$value.'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="author" class="col-sm-1 control-label">歌者</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="author" name="author" value="<?php echo $song->author; ?>"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="author" class="col-sm-1 control-label">影片1</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="link" value="<?php echo $song->link; ?>" />
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="link_title" value="<?php echo $song->link_title; ?>"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="author" class="col-sm-1 control-label">影片2</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="link2" value="<?php echo $song->link2; ?>" />
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="link2_title" value="<?php echo $song->link2_title; ?>"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="author" class="col-sm-1 control-label">影片3</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="link3" value="<?php echo $song->link3; ?>" />
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="link3_title" value="<?php echo $song->link3_title; ?>"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="author" class="col-sm-1 control-label">影片4</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="link4" value="<?php echo $song->link4; ?>" />
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="link4_title" value="<?php echo $song->link4_title; ?>"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="context" class="col-sm-1 control-label">主題歌曲介紹</label>
                                <div class="col-sm-10">
                                    <textarea id="context" name="context" rows="10" cols="80"><?php echo $song->context; ?></textarea>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label for="context" class="col-sm-1 control-label">歌單</label>
                                <div class="col-sm-10">
                                    <textarea id="lyric" name="lyric" rows="10" cols="80"><?php echo $song->lyric; ?></textarea>
                                </div>
                            </div>

                            <input type="hidden" name="id"  value="<?php echo $song->id; ?>"/>
                            <button type="button" id="save" class="btn btn-primary">儲存</button>
                            <button type="button" class="btn btn-default" onClick="location.href='/admin/Song'">取消</button>
                        </form>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col-->
        </div><!-- ./row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- CK Editor -->
<script src="<?php echo CK;?>ckeditor.js"></script>
<script>
$(function () {
    CKEDITOR.replace('context');
    CKEDITOR.replace('lyric');
    
    $('#save').click(function(){
        let str = "";
        let title = $('#title').val()
        let author = $('#author').val()
        let link = $('#link').val()
        let lyric = CKEDITOR.instances['lyric'].getData();
        if (title === '') {
            str += "請輸入名稱\n";
        } else if (author === '') {
            str += "請填寫歌者\n";
        } else if (link === '') {
            str += "請填寫影片連結\n";
        } else if (lyric === '') {
            str += "請填歌單\n";
        }
        
        if (str !== '') {
            window.alert(str)
            return false
        } else {
            $('#song-form').submit() 
        }  
    })
});
</script>
