<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>新增首頁</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 首頁</a></li>
            <li><a href="#">首頁管理</a></li>
            <li class="active">新增首頁</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-body pad">
                        <form action="/admin/Song/adding_song" id="song-form" class="form-horizontal" method="post"  enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="title" class="col-sm-1 control-label">歌曲名稱</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="title" name="title" placeholder="歌曲名稱"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="category_id" class="col-sm-1 control-label">歌曲分類</label>
                                <div class="col-sm-6">
                                    <?php 
                                    foreach($category as $key => $value) { 
                                        if($value->id!=6){
                                    ?>
                                        <input type="checkbox" value="<?php echo $value->id?>" name="category_id[]"><?php echo $value->title?>&nbsp;&nbsp;
                                    <?php 
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="tag_1" class="col-sm-1 control-label">語文</label>
                                <div class="col-sm-6">
                                    <select name="tag_1" class="form-control" id="tag_1">
                                    <?php foreach($tag_one as $key => $value) { ?>
                                        <option value="<?php echo $value->id?>"><?php echo $value->title?></option>
                                    <?php }?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="tag_3" class="col-sm-1 control-label">節奏</label>
                                <div class="col-sm-6">
                                    <select name="tag_3" class="form-control" id="tag_3">
                                    <?php foreach($tag_three as $key => $value) { ?>
                                        <option value="<?php echo $value->id?>"><?php echo $value->title?></option>
                                    <?php }?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="status" class="col-sm-1 control-label">狀態</label>
                                <div class="col-sm-6">
                                    <select name="status" class="form-control" id="status">
                                    <?php foreach($status as $key => $value) { ?>
                                        <option value="<?php echo $key?>"><?php echo $value?></option>
                                    <?php }?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="author" class="col-sm-1 control-label">歌者</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="author" name="author" placeholder="歌者"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="author" class="col-sm-1 control-label">影片連結</label>
                                <div class="col-sm-6">
                                    <input type="text" id="link" class="form-control" name="link" placeholder="影片連結"/>
                                </div>
                            </div>
                            <!--
                            <div class="form-group">
                                <label for="image_path" class="col-sm-1 control-label">圖片</label>
                                <div class="col-sm-6">
                                    <input type="file" name="image_path" class="form-control" placeholder="圖片">
                                </div>
                            </div>
                            -->
                            <div class="form-group">
                                <label for="context" class="col-sm-1 control-label">推薦內容</label>
                                <div class="col-sm-10">
                                    <textarea id="context" name="context" rows="10" cols="80"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="context" class="col-sm-1 control-label">歌詞</label>
                                <div class="col-sm-10">
                                    <textarea id="lyric"  name="lyric" rows="10" cols="80"></textarea>
                                </div>
                            </div>

                            <button type="button" id="save" class="btn btn-primary">建立</button>
                            <button type="button" class="btn btn-default" onclick="location.href='/admin/song'">取消</button>
                        </form>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col-->
        </div><!-- ./row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script src="<?php echo CK;?>ckeditor.js"></script>
<script>
$(function () {
    CKEDITOR.replace('context');
    CKEDITOR.replace('lyric');
    
    $('#save').click(function() {
        let str = "";
        let title = $('#title').val()
        let author = $('#author').val()
        let link = $('#link').val()
        let lyric = CKEDITOR.instances['lyric'].getData();
        //let tag_1 = $('#tag_1 option:selected').val()
        
        if (title === '') {
            str += "請輸入名稱\n";
        } else if (author === '') {
            str += "請填寫歌者\n";
        } else if (link === '') {
            str += "請填寫影片連結\n";
        } else if (lyric === '') {
            str += "請填歌詞\n";
        }
        
        if ("" != str) {
            window.alert(str);
            return false;
        } else {
            $('#song-form').submit(); 
        }     
    })
});
</script>
