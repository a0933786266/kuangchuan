      <div class="content-wrapper">
        <section class="content-header">
          <h1>新增首頁</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 首頁</a></li>
            <li><a href="#">首頁管理</a></li>
            <li class="active">新增首頁</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-body pad">
                  <form action="/admin/Home/adding_home" id="home-form" class="form-horizontal" method="post"  enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="name" class="col-sm-1 control-label">關於我們</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control name" id="name" name="name" placeholder="品名"/>
                      </div>
                    </div>

                    <div class="form-group">
                        <label for="content" class="col-sm-1 control-label">內容</label>
                        <div class="col-sm-10">
                            <textarea id="content" name="content" rows="10" cols="80"></textarea>
                        </div>
                    </div>

                    <button type="button" id="save" class="btn btn-primary">建立</button>
                    <button type="button" class="btn btn-default" onclick="location.href='/admin/home'">取消</button>
                  </form>
                  
                </div>
              </div><!-- /.box -->

            </div><!-- /.col-->
          </div><!-- ./row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
<style type="text/css">
.label_width{
    width:60px;
    padding:0px;
}
</style>
<script src="<?php echo CK;?>ckeditor.js"></script>
<script>
$(function () {
    CKEDITOR.replace('content');
    
    $('#save').click(function(){
        let str="";
        let name = $('#name').val();
        
        if(name===''){
            str+="請輸入名稱\n";
        }
        
        if(str !== '') {
            window.alert(str);
            return false;
        } else {
            $('#home-form').submit(); 
        }
        
    })
  });
</script>
