<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo AdminDist?>img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $user_name;?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <!--
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
              -->
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">功能選單列</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>網站前台</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="https://kc.milkcoffeehouse.com/Home/" target="_blank"><i class="fa fa-circle-o"></i>網站前台</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>網站設定</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <!--
                <li><a href="/admin/company"><i class="fa fa-circle-o"></i>公司</a></li>
                -->
                <li><a href="/admin/manager"><i class="fa fa-circle-o"></i>管理員</a></li>
                <li><a href="/admin/Story/editpage"><i class="fa fa-circle-o"></i>乳香世家故事</a></li>
                <li><a href="/admin/Fold/editpage"><i class="fa fa-circle-o"></i>故事圖片維護</a></li>
              </ul>
            </li>
            
            <!--
            <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>關於我們</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/admin/About"><i class="fa fa-circle-o"></i>關於我們</a></li>
                <li><a href="/admin/About/adding_about_page"><i class="fa fa-circle-o"></i>新增關於我們</a></li>
              </ul>
            </li>
            -->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>店家管理</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/admin/Store"><i class="fa fa-circle-o"></i>店家管理</a></li>
                <!-- <li><a href="/admin/StoreImport"><i class="fa fa-circle-o"></i>店家</a></li>
                 <li><a href="/admin/City"><i class="fa fa-circle-o"></i>縣市鄉鎮</a></li> -->
              </ul>
            </li>
            <!--
            <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>熱門話題</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/admin/Hot"><i class="fa fa-circle-o"></i>熱門話題</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>服務夥伴管理</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/admin/Partner"><i class="fa fa-circle-o"></i>夥伴</a></li>
                <li><a href="/admin/PartnerCategory"><i class="fa fa-circle-o"></i>夥伴分類</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>聯絡我們</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/admin/Contact"><i class="fa fa-circle-o"></i>聯絡我們</a></li>
              </ul>
            </li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>會員管理</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/admin/User"><i class="fa fa-circle-o"></i>會員</a></li>
              </ul>
            </li>
          -->
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
