<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="/resource/js/date.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>店家管理</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>店家管理</a></li>
            <li class="active">店家管理</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
<form class="form-inline"  method="post">
    <a class="btn btn-primary" href="/admin/Store/adding_store_page">新增店家</a>
  <div class="form-group">
    <label for="staticEmail2" class="sr-only">縣市</label>
    <select name="city" class="city form-control">
        <option value="All">全部</option>
        <?php
            foreach($citylist as $key => $value) {
                if($citySelected === $value->id) {
                    echo "<option selected value='". $value->id."' title='".$value->road_name."'>".$value->name."</option>";
                } else {
                    echo "<option value='". $value->id."' title='".$value->road_name."'>".$value->name."</option>";
                }
            }
        ?>
    </select>
  </div>
  <div class="form-group mx-sm-3">
    <label for="inputPassword2" class="sr-only">鄉鎮</label>
    <select name="town" class="town form-control"></select>
  </div>
  <button type="submit" class="btn btn-success ">查詢</button>
</form>
<br/>
<div class="row">
    <div class="col-md-2">
    <input type="text" placeholder="起始日期" value="<?php echo date("Y-m-01");?>" class="form-control" id="startdate" name="startdate">
    </div>
    <div class="col-md-2">
    <input type="text" placeholder="結束日期" value="<?php echo date("Y-m-d");?>" class="form-control" id="enddate" name="enddate">
    </div>
    <div class="col-md-2">
      <label for="staticEmail2" class="sr-only">日/月</label>
      <select name="mode" id="mode" class="city form-control">
        <option selected value="date">日</option>
        <option value="month">月</option>
      </select>
    </div>
    <div class="col-md-1">
        <button type="button" class="btn btn-success" onclick="exportFile()">匯出資料</button>
    </div>
    <div class="col-md-1">
        <div id="download"></div>
    </div>


</div>

                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped data_table">

                            <thead>
                                <tr>
                                    <th>編號</th>
                                    <th>圖名</th>
                                    <th>店名</th>
                                    <th>總瀏覽次數</th>
                                    <th>縣市鄉鎮</th>
                                    <th>經緯度</th>
                                    <th>電話</th>
                                    <th>地址</th>
                                    <th>推薦狀態</th>
                                    <th>刊登狀態</th>
                                    <th>編輯</th>
                                    <th>刪除</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $town = '';
                                $townList = array();
                                foreach ($store as $key => $row) {

                                    if($row->city!=''){
                                        $town =$citylist[$row->city-1]->road_name;
                                        $townList = explode("|",$town);
                                    }
                                    //print_r($townList);
                                    echo "<tr>";
                                    echo "<td>".$row->id."</td>";
                                    if(!empty($row->image_path)){
                                      echo "<td><img src='/upload/store/".$row->image_path."' width='150' /></td>";
                                    } else {
                                      echo "<td></td>";
                                    }
                                    echo "<td>".$row->name." ".$row->manager."</td>";
                                    echo "<td>".$row->views."</td>";
                                    if($row->city_name!='' && $row->town!=''){
                                        echo "<td>".$row->city_name.$townList[$row->town]."</td>";
                                    } else {
                                        echo "<td>".$row->city_name."</td>";
                                    }
                                     echo "<td>".$row->lat."</td>";
                                    echo "<td>".$row->phone."</td>";
                                    //echo "<td><input type='number' class='ranking' value='".$row->rank."' id='".$row->id."' style='width:50px;'/></td>";
                                    echo "<td>".$row->address."</td>";

                                    if($row->promote==1){
                                        echo "<td>推薦</td>";
                                    } else {
                                        echo "<td></td>";
                                    }
                                    if($row->status==1){
                                        echo "<td><i class='glyphicon glyphicon-ok'></i></td>";
                                    } else {
                                        echo "<td><i class='glyphicon glyphicon-remove'></i></td>";
                                    }

                                    if($row->category_id==6){
                                        echo "<td><button type='button' class='btn btn-sm btn-success' onclick=location.href='/admin/store/editing_classic_page/".$row->id."'>編輯</button></td>";
                                    } else {
                                        echo "<td><button type='button' class='btn btn-sm btn-success' onclick=location.href='/admin/store/edit_store_page/".$row->id."'>編輯</button></td>";
                                    }

                                    echo "<td><button type='button' class='btn btn-sm btn-danger' onclick=delete_store(".$row->id.")>刪除</button></td>";
                                    echo "</tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- DataTables -->
<script src="<?php echo AdminPlugins?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo AdminPlugins?>datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo URL_JS?>/data_table_component.js"></script>

<script language="javascript">
$(function () {
    $("#startdate,#enddate").datepicker(opt);

    exportFile = function () {
        $('#download').html("");
        let startdate = $("#startdate").val()
        let enddate = $("#enddate").val()
        let mode = $("#mode").val()

        $.ajax({
          method: "POST",
          url: "/admin/Store/exportData/",
          data:{
            startdate: startdate,
            enddate: enddate,
            mode: mode
          }
        }).success(function(res){
          let data = jQuery.parseJSON(res)
          let down = "<a target='_blink' href='/upload/export/"+data.filename+"'>下載檔案</a>";
          $('#download').html(down);
        });
    }

    openview = function(id){
        var x = screen.width/2 - 700/2;
        var y = screen.height/2 - 450/2;
        window.open('store/viewsStore', 'sharegplus','height=485,width=700,left='+x+',top='+y);
    }

    $('.city').change(function(){
        let val =$(this).val()
        let townList = $('.city option:selected').attr("title")
        if(val!=='All'){
          if(townList){
                townList = townList.split("|")
                townList.shift()


            let optionStr = '<option value="All">全部</option>'
            $.each(townList,function(idx,val){
                idx+=1
                optionStr += "<option value='" + idx + "'>" + val + "</option>";
            })
            $(".town").html(optionStr)
          }
        }
    }).trigger("change")

    delete_store = function(id, category_id){
        if (confirm("確定刪除此store嗎?")) {
            $.ajax({
                method: "POST",
  			    url: "/admin/store/delete_store/",
  			    data:{
  				    id:id
  			    }
  		    }).success(function(msg){
  		        if(msg!=0){
                    location.reload();
  			    }
  		    });
        } else {
		    return false;
	    }
    }
    /*
    $(document.body).on("keyup",".ranking",function(){
        let id=$(this).attr("id");
        $.ajax({
            method: "POST",
            url: "/admin/store/change_ranking",
            data:{
              id: id,
              rank: $(this).val()
            }
        }).success(function(res){
            window.alert('更新成功')
        })
    })

    $(document.body).on("change",".home",function(){
        let id=$(this).attr("id");
        $.ajax({
            method: "POST",
            url: "/admin/store/change_home_recommend",
            data:{
                id:id,
                status:$(this).val()
            }
        }).success(function(res){
            window.alert('更新成功')
        });
    });
    */
});
</script>
