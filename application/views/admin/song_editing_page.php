<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>編輯歌曲</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 歌曲</a></li>
            <li><a href="#">編輯歌曲管理</a></li>
            <li class="active">編輯歌曲</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-body pad">
                        <form action="/admin/Song/editing_song" id="song-form" class="form-horizontal" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="title" class="col-sm-1 control-label">歌曲名稱</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="title" name="title" value="<?php echo $song->title; ?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="category_id" class="col-sm-1 control-label">歌曲分類</label>
                                <div class="col-sm-6">
                                    <?php 
                                    $category_ids = explode(',', $song->category_id);
                                    foreach($category as $key => $value) { ?>
                                        <input type="checkbox" name="category_id[]" value="<?php echo $value->id?>"<?php if (in_array($value->id, $category_ids)) {echo 'checked';}?>><?php echo$value->title?>&nbsp;&nbsp;
                                    <?php }?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="tag_1" class="col-sm-1 control-label">語文</label>
                                <div class="col-sm-6">
                                    <select name="tag_1" class="form-control" id="tag_1">
                                        <?php foreach($tag_one as $key => $value) { ?>
                                        <option value="<?php echo $value->id?>"<?php if ($song->tag_1 === $value->id) {echo 'selected';}?>><?php echo$value->title?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label for="tag_3" class="col-sm-1 control-label">節奏</label>
                                <div class="col-sm-6">
                                    <select name="tag_3" class="form-control" id="tag_3">
                                        <?php foreach($tag_three as $key => $value) { ?>
                                        <option value="<?php echo $value->id?>"<?php if ($song->tag_3 === $value->id) {echo 'selected';}?>><?php echo$value->title?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="status" class="col-sm-1 control-label">狀態</label>
                                <div class="col-sm-6">
                                    <select name="status" class="form-control" id="status">
                                        <?php 
                                        foreach($status as $key => $value) { 
                                            if($song->status == $key){
                                                echo '<option value='.$key.' selected>'.$value.'</option>';
                                            } else {
                                                echo '<option value='.$key.'>'.$value.'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="author" class="col-sm-1 control-label">歌者</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="author" name="author" value="<?php echo $song->author; ?>"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="author" class="col-sm-1 control-label">影片連結</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="link" value="<?php echo $song->link; ?>" />
                                </div>
                            </div>
                            <!--
                            <div class="form-group">
                                <label for="image_path" class="col-sm-1 control-label">圖片(1140*443)</label>
                                <div class="col-sm-6">
                                    <input type="file" name="image_path" class="form-control" placeholder="圖片">
                                    <img src="<?php // echo '/upload/song/'.$song->image_path ?>" width="50%" alt="<?php // echo $song->image_path; ?>"/>
                                </div>
                            </div>
                            -->
                            <div class="form-group">
                                <label for="context" class="col-sm-1 control-label">推薦內容</label>
                                <div class="col-sm-10">
                                    <textarea id="context" name="context" rows="10" cols="80"><?php echo $song->context; ?></textarea>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label for="context" class="col-sm-1 control-label">歌詞</label>
                                <div class="col-sm-10">
                                    <textarea id="lyric" name="lyric" rows="10" cols="80"><?php echo $song->lyric; ?></textarea>
                                </div>
                            </div>

                            <input type="hidden" name="id"  value="<?php echo $song->id; ?>"/>
                            <button type="button" id="save" class="btn btn-primary">儲存</button>
                            <button type="button" class="btn btn-default" onClick="location.href='/admin/Song'">取消</button>
                        </form>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col-->
        </div><!-- ./row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- CK Editor -->
<script src="<?php echo CK;?>ckeditor.js"></script>
<script>
$(function () {
    CKEDITOR.replace('context');
    CKEDITOR.replace('lyric');
    
    $('#save').click(function(){
        let str = "";
        let title = $('#title').val()
        let author = $('#author').val()
        let link = $('#link').val()
        let lyric = CKEDITOR.instances['lyric'].getData();
        if (title === '') {
            str += "請輸入名稱\n";
        } else if (author === '') {
            str += "請填寫歌者\n";
        } else if (link === '') {
            str += "請填寫影片連結\n";
        } else if (lyric === '') {
            str += "請填歌詞\n";
        }
        
        if (str !== '') {
            window.alert(str)
            return false
        } else {
            $('#song-form').submit() 
        }  
    })
});
</script>
