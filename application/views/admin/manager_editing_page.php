
      <div class="content-wrapper">
        <section class="content-header">
          <h1>編輯成員</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">成員管理</a></li>
            <li class="active">編輯成員</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-body pad">  
                <form action="/admin/manager/editing_manager" class="form-horizontal" method="post" enctype="multipart/form-data">
                	<div class="form-group">
                      <label for="title" class="col-sm-1 control-label">姓名</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" name="name" value="<?php echo
$manager->name; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="title" class="col-sm-1 control-label">Email</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" name="email" value="<?php echo $manager->email; ?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="title" class="col-sm-1 control-label">密碼</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" name="password" value="<?php echo $manager->password; ?>">
                      </div>
                    </div>
                    <input type="hidden" name="id"  value="<?php echo $manager->id; ?>">
                    
                    <button type="submit" class="btn btn-primary">編輯成員</button>
                    <button type="button" class="btn btn-default" onClick="location.href='/admin/manager'">取消</button>
                  </form>
                </div>
              </div><!-- /.box -->

            </div><!-- /.col-->
          </div><!-- ./row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->