      <div class="content-wrapper">
        <section class="content-header">
          <h1>編輯首頁</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">首頁管理</a></li>
            <li class="active">編輯首頁</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-body pad">
                  <form action="/admin/Home/editing_home" id="home-form" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="name" class="col-sm-1 control-label">首頁名稱</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $home->name; ?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="cid" class="col-sm-1 control-label">廠商</label>
                        <div class="col-sm-6">
                            <select name="cid" class="form-control" id="cid">
                              <?php foreach($company as $key => $value) { ?>
                              <option value="<?php echo $value->id?>"<?php if ($home->cid === $value->id) {echo 'selected';}?>><?php echo$value->name?></option>
                              <?php }?>
                            </select>
                        </div>
                     </div>

                    <div class="form-group">
                        <label for="content" class="col-sm-1 control-label">產品介紹</label>
                        <div class="col-sm-10">
                            <textarea id="content" name="content" rows="10" cols="80"><?php echo $home->content; ?></textarea>
                        </div>
                    </div> 
                    <input type="hidden" name="id"  value="<?php echo $home->id; ?>"/>
                    
                    <button type="button" id="save" class="btn btn-primary">儲存產品</button>
                    <button type="button" class="btn btn-default" onClick="location.href='/admin/home'">取消</button>
                  </form>
                </div>
              </div><!-- /.box -->

            </div><!-- /.col-->
          </div><!-- ./row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
    <!-- CK Editor -->
<style type="text/css">
.label_width{
    width:60px;
    padding:0px;
}
</style>    
<script src="<?php echo CK;?>ckeditor.js"></script>
<script>
$(function () {
    CKEDITOR.replace('content');
    
    $('#save').click(function(){
        let str = "";
        let name = $('.name').eq(0).val();
        let price = $('.price').eq(0).val();
        
        if(name==""){
            str+="請輸入名稱\n";
        } else if(price==""){
            str+="請輸入價格\n";
        }
        
        if (str!="") {
            alert(str);
            return false;
        } else {
            $('#home-form').submit(); 
        }  
    })
});


</script>
