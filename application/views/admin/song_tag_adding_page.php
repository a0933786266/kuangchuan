<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>歌曲標籤</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 標籤</a></li>
            <li><a href="#">標籤管理</a></li>
            <li class="active">歌曲標籤</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-body pad">
<form action="/admin/SongTag/adding_song_tag" id="song_tag-form" class="form-horizontal" method="post"  enctype="multipart/form-data">
                            <div class="form-group">
                              <label for="title" class="col-sm-1 control-label">標籤名稱</label>
                              <div class="col-sm-6">
                                <input type="text" class="form-control title" id="title" name="title" placeholder="標籤名稱"/>
                              </div>
                            </div>

    <div class="form-group">
        <label for="parent_id" class="col-sm-1 control-label">分類</label>
        <div class="col-sm-6">
            <select name="parent_id" class="form-control" id="parent_id">
                <?php foreach ($song_tag_parent as $key => $value) {?>
                    <option value="<?php echo $value->id?>"><?php echo $value->title?></option>
                <?php }?>
            </select>
        </div>
    </div>

                            <button type="button" id="save" class="btn btn-primary">建立</button>
                            <button type="button" class="btn btn-default" onclick="location.href='/admin/SongTag'">取消</button>
                        </form>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col-->
        </div><!-- ./row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script src="<?php echo CK;?>ckeditor.js"></script>
<script>
$(function () {
    $('#save').click(function(){
        let str = "";
        let title = $('#title').val();
        
        if (title === '') {
            str+="請輸入標籤名稱\n";
        }
        
        if(str !== ''){
            window.alert(str);
            return false;
        } else {
            $('#song_tag-form').submit(); 
        }
    })
  });
</script>
