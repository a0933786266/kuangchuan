      <div class="content-wrapper">
        <section class="content-header">
          <h1>編輯故事圖片</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">故事圖片維護</a></li>
            <li class="active">故事圖片維護</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-body pad">
                  <form action="/admin/Fold/edit" id="home-form" class="form-horizontal" method="post" enctype="multipart/form-data">
                    
                    <div class="form-group">
                      <label for="name" class="col-sm-1 control-label">1.電腦版本圖片(510*300)</label>
                      <div class="col-sm-6">
                        <input type="file" class="form-control" name="photo1"/>
                        <img src="/upload/data/<?php echo $fold->val; ?>" width="300"/>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="name" class="col-sm-1 control-label">2.電腦版本圖片(510*300)</label>
                      <div class="col-sm-6">
                        <input type="file" class="form-control" name="photo2"/>
                        <img src="/upload/data/<?php echo $fold->val2; ?>" width="300"/>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="name" class="col-sm-1 control-label">3.電腦版本圖片(510*300)</label>
                      <div class="col-sm-6">
                        <input type="file" class="form-control" name="photo3"/>
                        <img src="/upload/data/<?php echo $fold->val3; ?>" width="300"/>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="name" class="col-sm-1 control-label">4.電腦版本圖片(510*300)</label>
                      <div class="col-sm-6">
                        <input type="file" class="form-control" name="photo4"/>
                        <img src="/upload/data/<?php echo $fold->val4; ?>" width="300"/>
                      </div>
                    </div>

                    <input type="hidden" name="id"  value="<?php echo $fold->id; ?>"/>
                    
                    <button type="submit" id="save" class="btn btn-primary">儲存</button>
                    <button type="button" class="btn btn-default" onClick="location.href='/admin/Fold/editpage'">取消</button>
                  </form>
                </div>
              </div><!-- /.box -->

            </div><!-- /.col-->
          </div><!-- ./row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
