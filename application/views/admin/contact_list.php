<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>聯絡資訊管理</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>管理</a></li>
            <li class="active">聯絡資訊管理</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped data_table">
                        <caption>
                        </caption>
                            <thead>
                                <tr>
                                    <th>編號</th>
                                    <th>姓名</th>
                                    <th>主題</th>
                                    <th>內容</th>
                                    <th>刪除</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                foreach ($contact as $key => $row) {
                                    echo "<tr>";
                                    echo "<td>".$row->id."</td>";
                                    echo "<td>".$row->name."</td>";
                                    echo "<td>".$row->title."</td>";
                                    echo "<td>".$row->context."</td>";
                                    echo "<td><button type='button' class='btn btn-sm btn-danger' onclick=delete_contact('".$row->id."')>刪除</button></td>";
                                    echo "</tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- DataTables -->
<script src="<?php echo AdminPlugins?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo AdminPlugins?>datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo URL_JS?>/data_table_component.js"></script>
<!-- page script -->
<script language="javascript">
$(function () {
    delete_contact=function(id){
        if (confirm("確定刪除此頁面嗎?")) {
            $.ajax({
                method: "POST",
  			    url: "/admin/Contact/delete_contact/",
  			    data:{
  				    id:id
  			    }
  		    }).success(function(msg){
  		        if(msg!=0){
                    location.reload(); 
  			    }
  		    });
        } else {
		    return false;
	    }
    }
    
});
</script>
