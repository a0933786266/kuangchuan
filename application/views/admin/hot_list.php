<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>熱門話題管理</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>管理</a></li>
            <li class="active">熱門話題管理</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped data_table">
                        <caption>
                            <a class="btn btn-primary pull-left" href="/admin/Hot/adding_hot_page">新增熱門話題</a>
                        </caption>
                            <thead>
                                <tr>
                                    <th>編號</th>
                                    <th>排序</th>
                                    <th>主題名稱</th>
                                    <th>狀態</th>
                                    <th>編輯</th>
                                    <th>刪除</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                foreach ($hot as $key => $row) {
                                    echo "<tr>";
                                    echo "<td>".$row->id."</td>";
                                    echo "<td><input type='number' class='ranking' value='".$row->rank."' id='".$row->id."' style='width:50px;'/></td>";
                                    echo "<td>".$row->title."</td>";
                                    echo "<td>";
                                    echo "<select id=".$row->id." class='status'>";
                                    if($row->status==1){
                                        echo "<option value='1' selected>顯示</option>";
                                        echo "<option value='0' >不顯示</option>";
                                    } else {
                                        echo "<option value='1' >顯示</option>";
                                        echo "<option value='0' selected>不顯示</option>";
                                    }
                                    echo "</select>";
                                    echo "</td>";
                                    echo "<td><button type='button' class='btn btn-sm btn-success' onclick=location.href='/admin/Hot/edit_hot_page/".$row->id."'>編輯</button></td>";
                                    echo "<td><button type='button' class='btn btn-sm btn-danger' onclick=delete_hot('".$row->id."')>刪除</button></td>";
                                    echo "</tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- DataTables -->
<script src="<?php echo AdminPlugins?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo AdminPlugins?>datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo URL_JS?>/data_table_component.js"></script>
<!-- page script -->
<script language="javascript">
$(function () {
    delete_hot = function(id){
        if (confirm("確定刪除此頁面嗎?")) {
            $.ajax({
                method: "POST",
  			    url: "/admin/Hot/delete_hot/",
  			    data:{
  				    id:id
  			    }
  		    }).success(function(msg){
  		        if(msg!=0){
                    location.reload(); 
  			    }
  		    });
        } else {
		    return false;
	    }
    }

    $(document.body).on("change",".status",function(){
        let id=$(this).attr("id");
        $.ajax({
            method: "POST",
            url: "/admin/Hot/change_status",
            data:{
              id: id,
              status: $(this).val()
            }
        }).success(function(res){
            window.alert('更新成功')
        })
    })
    
    $(document.body).on("keyup",".ranking",function(){
        let id=$(this).attr("id");
        $.ajax({
            method: "POST",
            url: "/admin/Hot/change_ranking",
            data:{
                id:id,
                rank:$(this).val()
            }
        }).success(function(res){
            window.alert('更新完畢');
        });
    });
});
</script>
