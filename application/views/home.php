<!doctype html>
<html lang="zh-tw">
  <head>
    <title>光泉乳香世家</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta NAME="description" CONTENT="乳香世家以精選的超優質生乳製成，不但含有比例完美的乳脂肪及完整豐富的營養成份，更有綿密的口感和層次豐富的鮮乳風味，每一口都喝得到香醇濃郁！承襲光泉60年最優質的酪農養殖技術，讓乳香世家擁有與美、日等乳業先進國家水準相當的生乳，品質更是超越國家標準，所以能讓人譽為最美味的鮮乳。">
<meta NAME="keywords" CONTENT="乳香世家,光泉乳香世家,光泉,牛奶,咖啡,鮮乳">
    <!--[if lt IE 9]>
      <script src="http://apps.bdimg.com/libs/html5shiv/3.7/html5shiv.min.js"></script>

      <script src="http://apps.bdimg.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="/resource/css/bootstrap.min.css"> -->

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" > -->

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
<link rel="stylesheet" href="/resource/css/base.css"/>
    <link rel="stylesheet" href="/css/my.css"/>
    <link rel="stylesheet" href="/css/mobile.css"/>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=908066355897581';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light bg-color fixed-top">
      <div class="container">
        <a class="navbar-brand offset-md-1" href="#">
          <img src="/resource/images/logo.png">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTop" aria-controls="navbarTop" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <!--justify-content-center-->
        <div class="collapse navbar-collapse offset-md-2" id="navbarTop">
          <ul class="navbar-nav">

            <li class="nav-item">
              <a class="nav-link" href="#video"><h6>乳香世家影片</h6></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#map"><h6>乳香咖啡地圖</h6></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#story"><h6>乳香世家故事</h6></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#fans" ><h6>到粉絲團去</h6></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#contact"><h6>聯絡我們</h6></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <main role="main" class="mainTop">

      <div class="container">

        <div id="video"></div>
        <br/><br/>
         <div class="d-flex justify-content-center">
            <figure class="figure">
              <img src="/resource/images/title1.png" class="img-fluid" />
            </figure>
         </div>

         <div id="caVideo" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner" role="listbox">

          <div class="carousel-item active">
            <div class="row">
              <div class="col-md-6 offset-md-2">
                <div class="embed-responsive embed-responsive-1by1 d-block d-sm-none">
                  <iframe class="videoBlock" width="560" height="315" src="https://www.youtube.com/embed/EM9zimy_33k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>

                <div class="d-none d-sm-block">
                  <iframe class="videoBlock" width="560" height="315" src="https://www.youtube.com/embed/EM9zimy_33k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              </div>

              <div class="col-md-3 d-none d-sm-block" style="padding-top: 125px;">
                  <figure class="figure">
                    <img src="/resource/images/video-right.png" class="img-fluid" width="80%" />
                  </figure>
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="row">
            <div class="col-md-6 offset-md-2">
              <div class="embed-responsive embed-responsive-1by1 d-block d-sm-none">
                <iframe class="videoBlock"  width="560" height="315" src="https://www.youtube.com/embed/dXQGf7MAm7s" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>

              <div class="d-none d-sm-block">
                <iframe class="videoBlock" width="560" height="315" src="https://www.youtube.com/embed/dXQGf7MAm7s" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>
              </div>

              <!--牛奶-->
              <div class="col-md-3 d-none d-sm-block" style="padding-top: 125px;">
                  <figure class="figure">
                    <img src="/resource/images/video-right.png" class="img-fluid" width="80%" />
                  </figure>
              </div>
            </div>
          </div>

	<div class="carousel-item">
              <div class="row">
                <div class="col-md-6 offset-md-2">

                  <div class="embed-responsive embed-responsive-1by1 d-block d-sm-none">
                        <iframe class="videoBlock"  width="560" height="315" src="https://www.youtube.com/embed/Zw_VZXMWdTc" frameborder="0" allow="autoplay; encrypted-media" allowfull
screen></iframe>
                </div>
                  <div class="d-none d-sm-block">
                        <iframe class="videoBlock" width="560" height="315" src="https://www.youtube.com/embed/Zw_VZXMWdTc" frameborder="0" allow="autoplay; encrypted-media" allowfulls
creen></iframe>
                 </div>
                </div>
                <!--牛奶-->
                  <div class="col-md-3 d-none d-sm-block" style="padding-top: 125px;">
                      <figure class="figure">
                        <img src="/resource/images/video-right.png" class="img-fluid" width="80%" />
                      </figure>
                  </div>
              </div>
            </div>

<div class="carousel-item">
              <div class="row">
                <div class="col-md-6 offset-md-2">
                  <div class="embed-responsive embed-responsive-1by1 d-block d-sm-none">
                        <iframe class="videoBlock"  width="560" height="315" src="https://www.youtube.com/embed/M9njz8lF19g" frameborder="0" allow="autoplay; encrypted-media" allowfull
screen></iframe>
                </div>
                  <div class="d-none d-sm-block">
                        <iframe class="videoBlock" width="560" height="315" src="https://www.youtube.com/embed/M9njz8lF19g" frameborder="0" allow="autoplay; encrypted-media" allowfulls
creen></iframe>
                 </div>
                </div>
                <!--牛奶-->
                  <div class="col-md-3 d-none d-sm-block" style="padding-top: 125px;">
                      <figure class="figure">
                        <img src="/resource/images/video-right.png" class="img-fluid" width="80%" />
                      </figure>
		</div>
              </div>
            </div>


            <!-- 2 -->
            <div class="carousel-item">
              <div class="row">
                <div class="col-md-6 offset-md-2">

                  <div class="embed-responsive embed-responsive-1by1 d-block d-sm-none">
                	<iframe class="videoBlock"  width="560" height="315" src="https://www.youtube.com/embed/o2VV5i_VvY8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		</div>

                  <div class="d-none d-sm-block">
                	<iframe class="videoBlock" width="560" height="315" src="https://www.youtube.com/embed/o2VV5i_VvY8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		 </div>
                </div>

                <!--牛奶-->
                  <div class="col-md-3 d-none d-sm-block" style="padding-top: 125px;">
                      <figure class="figure">
                        <img src="/resource/images/video-right.png" class="img-fluid" width="80%" />
                      </figure>
                  </div>
              </div>
            </div>

            <!-- 3 -->
            <div class="carousel-item">
              <div class="row">

                <div class="col-md-6 offset-md-2">

                  <div class="embed-responsive embed-responsive-1by1 d-block d-sm-none">
                    <iframe class="videoBlock " width="560" height="315" src="https://www.youtube.com/embed/ooEs0Ufb49Y" frameborder="0" allow="encr
            ypted-media" allowfullscreen></iframe>
            </div>

                  <div class=" d-none d-sm-block">
                  <iframe class="videoBlock " width="560" height="315" src="https://www.youtube.com/embed/ooEs0Ufb49Y" frameborder="0" allow="encr
            ypted-media" allowfullscreen></iframe>
            </div>
                </div>

                <!--牛奶-->
                  <div class="col-md-3 d-none d-sm-block" style="padding-top: 125px;">
                      <figure class="figure">
                        <img src="/resource/images/video-right.png" class="img-fluid" width="80%" />
                      </figure>
                  </div>
              </div>
            </div>

            <!-- second -->
            <div class="carousel-item">
              <div class="row">

                <div class="col-md-6 offset-md-2">

                  <div class="embed-responsive embed-responsive-1by1 d-block d-sm-none">
                   	<iframe class="videoBlock " width="560" height="315" src="https://www.youtube.com/embed/ooEs0Ufb49Y" frameborder="0" allow="encr
ypted-media" allowfullscreen></iframe>
		</div>

                  <div class=" d-none d-sm-block">
                	<iframe class="videoBlock " width="560" height="315" src="https://www.youtube.com/embed/ooEs0Ufb49Y" frameborder="0" allow="encr
ypted-media" allowfullscreen></iframe>
	  	  </div>
                </div>

                <!--牛奶-->
                  <div class="col-md-3 d-none d-sm-block" style="padding-top: 125px;">
                      <figure class="figure">
                        <img src="/resource/images/video-right.png" class="img-fluid" width="80%" />
                      </figure>
                  </div>
              </div>
            </div>

          </div>
          <a class="carousel-control-prev" href="#caVideo" role="button" data-slide="prev">
            <div style="font-size:3em; color:#545454">
              <i class="fas fa-arrow-circle-left"></i>
            </div>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#caVideo"role="button" data-slide="next">

            <div style="font-size:3em; color:#545454">
              <i class="fas fa-arrow-circle-right"></i>
            </div>
            <span class="sr-only">Next</span>
          </a>
        </div>

        <br/><br/>
        <!--影片結束-->
        <div class="d-flex justify-content-center">
            <figure class="figure">
              <img src="/resource/images/title2.png" class="img-fluid" />
            </figure>
        </div>
        <br/>
        <div id="map" style="height: 500px;"></div>
        <br/><br/>

        <div class="d-flex justify-content-center">
            <div class="p-2">
              <form class="form-inline">
                <div class="form-group">
                  <select name="city" class="city form-control" style="margin-right: 5px;">
        <?php
            foreach ($citylist as $key => $value) {
                if ($value->id !=65) {
                    echo "<option value='". $value->id."' title='".$value->road_name."'>".$value->name."</option>";
                }
            }
        ?>
                  </select>

                </div>
                <div class="form-group">
                  <select name="town" class="town form-control" style="margin-right: 5px;"></select>
                </div>
                <div class="form-group">
                  <button type="button" id="searching" class="btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                    查詢
                  </button>
                </div>
              </form>
          </div>
      </div>

      <section>
        <div id="storeData"></div>
      </section>
      <br/>


      <div id="story"></div>
      <br/>
      <div class="d-flex justify-content-center">
        <figure class="figure">
          <img src="/resource/images/title3.png" class="img-fluid" />
        </figure>
      </div>
      <br/>
      <div class="d-flex justify-content-center">
          <span class=" d-block d-sm-none homestory" style="font-family:DFKai-sb; background-image: url('/resource/images/title6-bk.png');text-align:center;padding:1px; color:#716b6b;line-height: 16px;font-size: 12px;width: 100%">
            <span style="display:block; margin-left: 15%;">
              <br/>
              乳香世家採用日本同步脫氧保鮮技術，<br/>
              大幅減少氧氣對鮮乳風味的影響，<br/>
              口感香醇濃郁玩轉飲品，完美交融的極致美味。<br/>
            </span>
        		<span style="font-size: 16px;color:#103d98;margin-left:15%; line-height: 22px;display:block;margin-top:7px;">
              <span style="font-size: 20px; margin-bottom:3px;">香醇濃郁 ◆ 極致美味</span><br/>
              <span>- 玩轉味蕾季 鮮乳萬種情 -</span>
            </span>
            <br/>
        	</span>
          <!-- <img src="/upload/data/" class="img-fluid" /> -->
        <!--
	<figure class="figure d-block d-sm-none ">
          <img src="/upload/data/" class="img-fluid" />
        </figure>
	-->
        <figure class="figure d-none d-sm-block">
          <img src="/upload/data/<?php echo $story->val2;?>" class="img-fluid" />
        </figure>
      </div>
      <br/>

      <!-- 折疊圖片 -->
      <ul class="songpic-imglist list-inline">
        <li class="list-inline-item">
          <figure class="d-block d-sm-none">
              <img src="/upload/data/<?php echo $fold->val;?>" class="img-fluid"/>
          </figure>

          <figure class="pic d-none d-sm-block">
              <img src="/upload/data/<?php echo $fold->val;?>" />
          </figure>
        </li>

        <li class="list-inline-item">
          <figure class="d-block d-sm-none">
              <img src="/upload/data/<?php echo $fold->val2;?>" class="img-fluid" />
          </figure>

          <figure class="pic d-none d-sm-block">
              <img src="/upload/data/<?php echo $fold->val2;?>" />
          </figure>
        </li>

        <li class="list-inline-item">
          <figure class="d-block d-sm-none">
              <img src="/upload/data/<?php echo $fold->val3;?>" class="img-fluid"/>
          </figure>

          <figure class="pic d-none d-sm-block">
              <img src="/upload/data/<?php echo $fold->val3;?>" />
          </figure>
        </li>

        <li class="list-inline-item">
          <figure class="d-block d-sm-none">
              <img src="/upload/data/<?php echo $fold->val4;?>" class="img-fluid"/>
          </figure>

          <figure class="pic d-none d-sm-block">
              <img src="/upload/data/<?php echo $fold->val4;?>" />
          </figure>
        </li>
      </ul>


      <br/>

      <div id="fans" class="d-flex justify-content-center">
        <center>
            <h5 style="color:#513d32;font-size: 22px;" class="d-none d-sm-block">
            到乳香世家enjoy生活+粉絲團，你可以更了解乳香咖啡背後的秘密 ...
            </h5>
            <h5 style="color:#513d32;font-size: 22px;" class="d-block d-sm-none">
              到乳香世家enjoy生活+粉絲團，<br/>你可以更了解乳香咖啡背後的秘密 ...
            </h5>
        </center>
      </div>
      <br/>

      <div class="fb-page d-flex justify-content-center" data-href="https://www.facebook.com/enjoy.milkhouse/" data-tabs="timeline" data-width="500" data-height="300" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/enjoy.milkhouse/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/enjoy.milkhouse/">乳香世家 enjoy生活+</a></blockquote></div>

      <br/>
      <div class="d-flex justify-content-center">
        <figure class="figure">
          <img src="/resource/images/title4.png" class="img-fluid" />
        </figure>
      </div>
      <br/>
      <div class="row">
        <!--左側-->
        <div class="col-md-4">
          <form>
            <div class="form-group">
              <!-- <figure class="figure">
                <img src="/resource/images/contactus.png" class=" img-fluid"/>
              </figure> -->
                  <p style="color: #333;">
                    你的咖啡店也想加入乳香世家台灣地圖嗎？<br/>歡迎留下連絡資訊，將有專人和您聯絡。
                  </p>
            </div>

              <br/>
            <div class="form-group row" id="contact">

              <!--
              <label for="name" class="col-sm-2 col-form-label">姓名</label>
            -->
              <div class="col-sm-12">
                <input type="text" style="opacity:0.5;" class="form-control cus-input" name="manager" id="manager" placeholder="姓名 :">
              </div>
            </div>
            <div class="form-group">
              <!--
              <label for="inputPassword" class="col-sm-2 col-form-label">咖啡店名</label>
              -->
                <input type="text" style="opacity:0.5;" class="form-control cus-input" id="name" placeholder="咖啡店名 :">
            </div>
            <div class="form-group">
              <!--
              <label for="inputPassword" class="col-sm-2 col-form-label">咖啡店地址</label>
              -->
                <textarea style="opacity:0.5;" class="form-control cus-input" id="address" rows="3" placeholder="咖啡店地址 :"></textarea>
            </div>
            <div class="form-group">
              <!--
              <label for="inputPassword" class="col-sm-2 col-form-label">手機號碼</label>
              -->
                <input type="text" style="opacity:0.5;" class="form-control cus-input" id="phone" placeholder="手機號碼 :">
            </div>
            <div class="form-group">
                <center>
                  <button type="button" class="btn btn-default send-btn">寄出</button>
                </center>
            </div>
          </form>

        </div>

        <!--右側-->
        <div class="col-md-8">
            <figure class="figure">
              <img src="/resource/images/contact.png" class="img-fluid"/>
            </figure>
        </div>
      </div>

      <br/>
        <div class="d-flex justify-content-center align_center">
          <div class="center fansBtn">
            <a href="#video">
              <h6 id="fanstop">TOP</h6>
            </a>
          </div>
        </div>
      <br/>

</div> <!-- /container -->

    </main>


 <!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script> -->


  </body>
</html>

<script>
// window.addEventListener ('load', function () {
// var map = document.querySelectorAll('#taiwanMap area');
// for (var i=0; i<map.length; i++) {
//    if (map[i].addEventListener) {
//    map[i].addEventListener('touchstart', swap, false);
//    map[i].addEventListener('mouseover', swap, false);
//    }
// }

// function swap(ev) {
//     var x = event.clientX;     // Get the horizontal coordinate
//     var y = event.clientY;     // Get the vertical coordinate
//     var coor = "X coords: " + x + ", Y coords: " + y;
//    var lan = this.getAttribute('alt');
//    alert('touch ' + lan + coor)
//    return false;
// }

// })

let map;


  initMap = function () {
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 24.9896539, lng: 121.42121399999999},
      zoom: 8
    });

    //目前位置
    /*
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        map.setCenter(pos);
        autoMarker();
      }, function() {
        handleLocationError(true, infoWindow, map.getCenter());
      });
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infoWindow, map.getCenter());
    }
    */
  }


 $(function(){
    $('.carousel').carousel({
      interval: 600000
    })
//    let key = 0
//     $('#searching').click(function(){
//          let city = $('.city option:selected').val()
//          let town = $('.town option:selected').val()
//           $.ajax({
//             method: "POST",
//             url: "/api/Store/getStore",
//             data:{
//               city:city,
//               town:town,
//             },
//             error: function(xhr) {
//               console.log('Ajax request 發生錯誤');
//             },
//             success: function(res) {
//                 //let data = jQuery.parseJSON(res);
//                 // console.log('data:' + res.store.length);
//                 let rs =''
//                 if(res.store.length>0){

//     rs += '<div id="carouselMap" class="carousel slide" data-ride="carousel">'
//     rs +='<ol class="carousel-indicators">'
//       $.each(res.store, function( idx, val ) {
//         if (idx%6 === 0) {
//           if (idx === 0) {
//             rs +='<li data-target="#carouselMap" data-slide-to="0" class="active"></li>'
//           } else {
//             rs +='<li data-target="#carouselMap" data-slide-to="'+idx+'"></li>'
//           }

//         }
//       })
//     rs +='</ol>'
//       rs +='<div class="carousel-inner">'
//       $.each(res.store, function( idx, val ) {
//           if (idx%6 === 0) {
//             if (idx === 0) {
//               rs +='<div class="carousel-item active">'
//             } else {
//               rs +='<div class="carousel-item">'
//             }
//             rs +='<div class="row">'
//           }
//               rs +='<div class="col-md-6 border border-white">'
//                 rs +='<div class="row">'
//                   rs +='<div class="col-md-4">'
//                   rs +='<img src="/upload/store/' + val.image_path + '" class="img-responsive rounded storeImage" alt="' + val.name + '" width="200">'
//                   rs +='</div>'
//                   rs +='<div class="col-md-8">'
//                   rs +='<h4 class="store-title">' + val.name + '</h4>'
//                   if (val.context !==null && val.context !=='null') {
//                     rs +='<p class="lead">' + val.context + '</p>'
//                   }
//                   rs +='<p>電話: <a class="tel" href="tel:'+val.phone + '">' + val.phone + '</a></p>'
//                   rs +='<p>地址: <a  class="address" target="_blink" href="http://maps.google.com/maps?q='+val.address+'">' + val.address + '</a></p>'
//                   if (val.bshour !==null && val.bshour !=='null') {
//                     rs +='<p>營業時間: ' + val.bshour + '</p>'
//                   }
//                   rs +='</div>'
//                 rs +='</div>'
//               rs +='</div>'
//           fkey = idx+1
//           if(idx%5===0 && idx>1 || res.store.length === fkey) {
//             rs +='</div>'
//             rs +='</div>'
//           }
//       });
//       rs +='</div>'
// rs +='<a class="carousel-control-prev" href="#carouselMap" role="button" data-slide="prev">'
//   rs +='  <span class="carousel-control-prev-icon" aria-hidden="true"></span>'
//   rs +='  <span class="sr-only">Previous</span>'
//   rs +='</a>'
//   rs +='<a class="carousel-control-next" href="#carouselMap" role="button" data-slide="next">'
//   rs +='  <span class="carousel-control-next-icon" aria-hidden="true"></span>'
//   rs +='  <span class="sr-only">Next</span>'
//   rs +='</a>'
// rs +='</div>'
//                   $('#storeData').html(rs)
//                 } else {
//                   if (key !== 0) {
//                     window.alert('目前該區域沒有店家資訊，請重新查詢。')
//                   }
//                   key++
//                   $('#storeData').html('')
//                 }

//             }
//           });
//     }).trigger('click')

    $('.send-btn').click(function(){
        let manager = $('#manager').val()
        let name = $('#name').val()
        let address = $('#address').val()
        let phone = $('#phone').val()
        let str =''
        if (manager === '') {
            str += "請輸入您的姓名\n";
        } else if (name === '') {
            str += "請填寫店名\n";
        } else if (address === '') {
            str += "請填寫地址\n";
        }

        if (str !== '') {
            window.alert(str);
            return false;
        } else {
            $.ajax({
              method: "POST",
              url: "/api/Store/AddStore",
              data:{
                manager:manager,
                name:name,
                address:address,
                phone:phone
              },
              error: function(xhr) {
                window.alert('Ajax request 發生錯誤'+JSON.stringify(xhr));
              },
              success: function(res) {
                //let obj = jQuery.parseJSON(res);
                console.log('data:' + res);
                if(res.status === 200) {
                  window.alert(' 謝謝您提供的資訊，我們將盡快將資訊提供給相關人員處理。')
                } else {
                  window.alert('填寫資料失敗，請重新填寫。')
                }
              }
            });
        }
    })

  $('.city').change(function(){
      let townList = $('.city option:selected').attr("title")
          townList = townList.split("|")
          townList.shift()

      let optionStr = ''
      $.each(townList,function(idx,val){
          idx+=1
          optionStr += "<option value='" + idx + "'>" + val + "</option>";
      })
      $(".town").html(optionStr)
  }).delay(1000).trigger("change")

  let markers = []
  let bounds = new google.maps.LatLngBounds();
  /*
  function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    }
  */

  //地址轉經緯度
  // getLatLngByAddr = function (address) {
  //     var geocoder = new google.maps.Geocoder();  //定義一個Geocoder物件
  //     geocoder.geocode({ address: address},   //設定地址的字串
  //         function(results, status) {         //callback function
  //             if (status == google.maps.GeocoderStatus.OK) {    //判斷狀態
  //                 // alert(results[0].geometry.location);
  //                 //取得座標
  //                 let lat = results[0].geometry.location.lat();
  //         let lng = results[0].geometry.location.lng();
  //         console.log(lat + ', ' +lng)
  //         let LatLng = results[0].geometry.location
  //                 newMarker(LatLng)
  //             } else {
  //                 alert('Error');
  //             }
  //       }
  //   );
  // }

  // newMarker = function (myLatLng){
  //   var marker = new google.maps.Marker({
  //     position: myLatLng,
  //     map: map,
  //     title: 'Hello World!'
  //   });
  //   marker.addListener('click', function() {
  //     infowindow.open(map, marker);
  //   });
  // }

  let flag = {
  	times:0
  }

  let infoWindow = new google.maps.InfoWindow(), marker;

  //搜尋時取資料用
  $('#searching').click(function(){
    let city = $('.city option:selected').val()
    let town = $('.town option:selected').val()
    loadingStore(city,town);
  })

  loadingStore = function(city,town){
  	$.ajax({
      method: "POST",
      url: "/api/Store/getStore",
      data:{
        city:city,
        town:town,
      },
      error: function(xhr) {
        console.log('Ajax request 發生錯誤');
      },
      success: function(res) {
        let data = res.store.length
        if (data <= 0) {
          window.alert('目前該區域沒有店家資訊，請重新查詢。')
        }
        // console.log('data:' + JSON.stringify(res.store));
        clearMarkers()
        makerData(res.store)
        flag.times = 1
      }
    });
  }

  makerData = function (data) {
    let dataRow = []
    let content = []

    for (var i = 0; i < data.length; i++) {

      if (typeof data[i].name !== 'undefined' && data[i].lat !== '' && data[i].lng !== '') {
         // console.log('data=====:' + data[i].name);
         // console.log('data=====:' + data[i].lat +'===='+ data[i].lng);
        let row = {
          name:data[i].name,
          id: data[i].id,
          promote: parseInt(data[i].promote),
          lat: parseFloat(data[i].lat),
          lng: parseFloat(data[i].lng)
        }
        dataRow.push(row)

        let str = '<div id="map-content">'+
          '<div id="siteNotice">'+
          '</div>'+
          '<h4 class="store-title">' + data[i].name + '</h4>'+
          '<div class="store">'+
          '<p>'+data[i].context+'</p>'+
          '<div class="phone"><strong>電話 : </strong> <a class="tel" href="tel:'+data[i].phone + '">'+data[i].phone+'</a></div>'+
          '<div class="address"><strong>地址 : </strong> '+data[i].address+'</div>'+
          '<div class="address"><strong>營業時間 : </strong> '+data[i].bshour+'</div><br/>';
          if (data[i].image_path !==null) {
            str+='<figure class="figure">'+
            '<img class="img-fluid" style="max-width: 300px;" src="/upload/store/'+data[i].image_path+'" />'+
            '</figure>';
          }
          str+=
          '</div>'+
          '</div>';
         content.push(str);
      }
    }
    // console.log('data:' + dataRow.length +'===' +JSON.stringify(dataRow));
    autoMarker(dataRow,content);
  }


  function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
    bounds = new google.maps.LatLngBounds();
  }

  function clearMarkers() {
    return setMapOnAll(null);
  }

  function autoMarker (dataRow,content) {
    for (var i = 0; i < dataRow.length; i++) {
      let row = dataRow[i];
      let position = new google.maps.LatLng(row.lat, row.lng);
        bounds.extend(position);
      let image = '';
      if (dataRow[i].promote === 1) {
        image = '../resource/images/cap-gold.png';
      } else {
        image = '../resource/images/cap-blue.png';
      }

      let marker = new google.maps.Marker({
        position: {lat: row.lat, lng: row.lng},
        map: map,
        title: row.name,
        icon: image
      });
      markers.push(marker);
      /*
      var infowindow = new google.maps.InfoWindow({
      content: contentString
      });

      marker.addListener('click', function() {
      infowindow.open(map, marker);
      });
      */

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
              infoWindow.setContent(content[i]);
              infoWindow.open(map, marker);
              viewStore(dataRow[i].id)

          }
      })(marker, i));

    }
   	map.fitBounds(bounds);
    map.panToBounds(bounds);
  }

  function viewStore(id) {
    $.ajax({
      method: "POST",
      url: "/api/Store/viewStore",
      data:{
        id:id
      },
      error: function(xhr) {
        console.log('Ajax request 發生錯誤');
      },
      success: function(res) {
        //let data = res.store.length
      }
    });
  }
  loadingStore()

  $('.nav-link').click(function(){
      $('.navbar-toggler').trigger('click')
  })
})
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0awVbvNlfrp5hABP_jmCtWrRLq40H8iM&callback=initMap"
></script>
