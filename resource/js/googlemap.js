
	let map;
	let bounds = new google.maps.LatLngBounds();
	
	initMap = function () {
		map = new google.maps.Map($('#map'), {
		  center: {lat: 24.9896539, lng: 121.42121399999999},
		  zoom: 10
		});
		// autoMarker();

		//目前位置
		/*
		if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            map.setCenter(pos);
          	autoMarker();
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
        */
	}
	/*
	function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    }
    */

    //地址轉經緯度
	getLatLngByAddr = function (address) {   
	    var geocoder = new google.maps.Geocoder();  //定義一個Geocoder物件   
	    geocoder.geocode({ address: address},		//設定地址的字串   
	        function(results, status) {    			//callback function   
	            if (status == google.maps.GeocoderStatus.OK) {    //判斷狀態   
	                // alert(results[0].geometry.location);
	                //取得座標
	                let lat = results[0].geometry.location.lat();   
					let lng = results[0].geometry.location.lng();
					console.log(lat + ', ' +lng)
					let LatLng = results[0].geometry.location
	                newMarker(LatLng)                                
	            } else {   
	                alert('Error');   
	            }   
	      }   
	 	);  
	}

	newMarker = function (myLatLng){
	  var marker = new google.maps.Marker({
	    position: myLatLng,
	    map: map,
	    title: 'Hello World!'
	  });
	  marker.addListener('click', function() {
	    infowindow.open(map, marker);
	  });
	}

	let dataRow = content = [];
	let infoWindow = new google.maps.InfoWindow(), marker;

	autoMarker = function () {
		for (var i = 0; i < dataRow.length; i++) {
          let row = dataRow[i];
          var position = new google.maps.LatLng(row.lat, row.lng);
        	  bounds.extend(position);
	          marker = new google.maps.Marker({
			    position: {lat: row.lat, lng: row.lng},
			    map: map,
			    title: row.name
			  });		  
/*
var infowindow = new google.maps.InfoWindow({
content: contentString
});

marker.addListener('click', function() {
infowindow.open(map, marker);
});
*/
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
	            return function() {
	                infoWindow.setContent(content[i]);
	                infoWindow.open(map, marker);
	            }
	        })(marker, i));
			map.fitBounds(bounds);
        }
	}